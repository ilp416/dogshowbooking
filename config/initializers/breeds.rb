# frozen_string_literal: true

breeds_data = YAML.safe_load File.read Rails.root.join 'config/breeds.yml'

breeds_hash = {}
breeds_data.each do |group, breeds|
  breeds.each do |key, attrs|
    attrs = { key: key, group: group }.merge attrs || {}
    breeds_hash.merge! key => attrs
  end
end
BREED_LIST = breeds_hash.deep_symbolize_keys
