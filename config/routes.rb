# frozen_string_literal: true
require 'sidekiq/web'

Rails.application.routes.draw do
  mount Sidekiq::Web => '/sidekiq'
  get '/privacy_policy', to: 'info#privacy_policy', as: :privacy_policy
  get 'for_clubs', to: 'for_clubs#index'
  resources :payments, only: %i[index create]
  resources :catalogs, only: %i[index show]
  resources :registrations
  resources :dogs do
    get :search, on: :collection
    member do
      get :registrations
      post :registrations, to: 'dogs#register'
    end
  end

  devise_for :users, controllers: {
    omniauth_callbacks: 'users/omniauth_callbacks',
    sessions: 'users/sessions'
  }
  devise_scope :user do
    get 'signin', to: 'devise/sessions#new'
    get 'signup', to: 'devise/registrations#new'
    get 'signout', to: 'devise/sessions#destroy'
  end
  resources :users, only: %i[new create update destroy] do
    get :update_profile, on: :member
    post :facebook_user_deletion
    get :deletion_status
  end
  get '/update_profile' => 'users#update_profile'
  post '/update_profile' => 'users#update'
  patch '/update_profile' => 'users#update'

  namespace :admin do
    resources :users
    resources :events do
      put :gen_catalog, on: :member
      put :drop_catalog, on: :member
      put :close_registration, on: :member
      put :reopen_registration, on: :member
      put :publish_catalog, on: :member
      put :hide_catalog, on: :member
      resource :catalog, only: %i[show update] do
        get :diploms, on: :member
        get :cards, on: :member
        get :additional_list, on: :member
      end
    end
    resource :catalog, only: :show
    resources :registrations do
      get :payments, on: :collection
    end
    resources :dogs
  end

  resources :breeds, only: :index do
    get :autocomplete_title, on: :collection
    get :autocomplete_colors, on: :collection
  end
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  root to: 'for_clubs#index'
end
