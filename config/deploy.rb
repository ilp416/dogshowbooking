# frozen_string_literal: true

# config valid for current version and patch releases of Capistrano
lock '~> 3.16.0'

set :application, 'mydog.show'
set :repo_url, 'git@gitlab.com:ilp416/dogshowbooking.git'

# Default branch is :master
# ask :branch, `git rev-parse --abbrev-ref HEAD`.chomp

# Default deploy_to directory is /var/www/my_app_name
# set :deploy_to, "/var/www/my_app_name"
set :deploy_to, '/home/deploy/www/mydog.show'

# Default value for :format is :airbrussh.
# set :format, :airbrussh

# You can configure the Airbrussh format using :format_options.
# These are the defaults.
# set :format_options, command_output: true, log_file: "log/capistrano.log", color: :auto, truncate: :auto

# Default value for :pty is false
# set :pty, true

# Default value for :linked_files is []
# append :linked_files, "config/database.yml"
append :linked_files, 'config/database.yml', 'config/credentials.yml.enc',
  'config/google-cloud-service.json'

# Default value for linked_dirs is []
# append :linked_dirs, "log", "tmp/pids", "tmp/cache", "tmp/sockets", "public/system"
append :linked_dirs, 'log', 'tmp/pids', 'tmp/cache', 'tmp/sockets',
       'public/system', 'storage', 'node_modules'

# set :assets_prefix, 'packs' # Assets are located in /packs/
# set :keep_assets, 3 # Automatically remove stale assets
# set :assets_manifests, lambda { # Tell Capistrano-Rails how to find the Webpacker manifests
#   [release_path.join('public', fetch(:assets_prefix), 'manifest.json')]
# }

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Default value for local_user is ENV['USER']
# set :local_user, -> { `git config user.name`.chomp }
# set :default_env, {
#    PATH: '$HOME/.nvm/versions/node/v15.14.0/bin/:$PATH',
#    NODE_ENVIRONMENT: 'production'
# }
set :default_env, {
   PATH: '$HOME/.nvm/versions/node/v14.18.0/bin:PATH',
   NODE_ENVIRONMENT: 'production'
}

# Default value for keep_releases is 5
# set :keep_releases, 5

# Uncomment the following to require manually verifying the host key before first deploy.
# set :ssh_options, verify_host_key: :secure
#
set :rvm_ruby_version, 'ruby-2.7.4'
set :default_env, { rvm_bin_path: '~/.rvm/bin' }
set :ssh_options, { forward_agent: true }
after 'deploy:updated', 'newrelic:notice_deployment'
set :nvm_type, :user # or :system, depends on your nvm setup
set :nvm_node, 'v14.18.0'
set :nvm_map_bins, %w{node npm yarn}
