# frozen_string_literal: true

require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Mydogshow
  class Application < Rails::Application
    config.time_zone = 'Europe/Minsk'
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 6.0

    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration can go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded after loading
    # the framework and any gems in your application.

    config.sass.preferred_syntax = :sass

    config.react.camelize_props = true

    config.generators do |g|
      g.test_framework nil
      g.assets false
      g.helper false
      g.stylesheets false
      g.javascripts false
    end

    config.i18n.load_path += Dir[Rails.root.join('config', 'locales', '**', '*.{rb,yml}')]
    config.i18n.available_locales = %i[ru en]
    config.i18n.default_locale = :ru

    config.eager_load_paths << "#{Rails.root}/lib"

    # lets your API users pass in and receive camelCased or dash-cased keys,
    # while your Rails app receives and produces snake_cased ones
    config.middleware.use OliveBranch::Middleware
  end
end
