# frozen_string_literal: true

require 'rails_helper'

describe RegistrationsForPaymentQuery do
  subject { described_class.call user }
  describe 'returns user\'s registrations with right order' do
    let!(:user) { create :user }
    let(:club1) { create :club }
    let(:dog1) { create :dog }
    let(:dog2) { create :dog }
    let(:event1) { create :event, club: club1, starts_on: tomorrow }
    let(:event2) { create :event, club: club1, starts_on: tomorrow }
    let(:event5) { create :event, club: club1, starts_on: a_week_later }
    let(:event6) { create :event, club: club1, starts_on: a_week_later }
    let(:club2) { create :club }
    let(:event3) { create :event, club: club2, starts_on: tomorrow }
    let(:event4) { create :event, club: club2, starts_on: tomorrow }
    let(:tomorrow) { Time.zone.tomorrow }
    let(:a_week_later) { Time.zone.today + 1.week }

    let(:date1_club1_dog1_event1) do
      create :registration, event: event1, dog: dog1, user: user
    end
    let(:date1_club1_dog1_event2) do
      create :registration, event: event2, dog: dog1, user: user
    end
    let(:date1_club2_dog1_event3) do
      create :registration, event: event3, dog: dog1, user: user
    end
    let(:date1_club1_dog2_event1) do
      create :registration, event: event1, dog: dog2, user: user
    end
    let(:date1_club1_dog2_event2) do
      create :registration, event: event2, dog: dog2, user: user
    end
    let(:date1_club2_dog2_event3) do
      create :registration, event: event3, dog: dog2, user: user
    end
    let(:date2_club1_dog1_event5) do
      create :registration, event: event5, dog: dog1, user: user
    end

    it do
      is_expected.to eq [
        date1_club1_dog1_event1,
        date1_club1_dog1_event2,
        date1_club1_dog2_event1,
        date1_club1_dog2_event2,
        date1_club2_dog1_event3,
        date1_club2_dog2_event3,
        date2_club1_dog1_event5
      ]
    end
  end
end
