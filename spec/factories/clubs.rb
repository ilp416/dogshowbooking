# frozen_string_literal: true

FactoryBot.define do
  factory :club do
    name { Faker::Space.star }
    fullname { "ABBR #{Faker::Space.star}" }
    slug { name.parameterize }
  end
end
