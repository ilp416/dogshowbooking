# frozen_string_literal: true

FactoryBot.define do
  factory :event do
    name { Faker::Games::SuperMario.game }
    starts_on { Date.today + 7.days }
    club
  end
end
