# frozen_string_literal: true

FactoryBot.define do
  factory :registration do
    dog
    event
    user { dog.user }
    show_class { Registration.show_class.values.sample }
  end
end
