# frozen_string_literal: true

FactoryBot.define do
  factory :dog do
    name { Faker::Artist.name }
    breed_id { :chinese_crested_dog }
    hair_type_id { :hairless }
    sex { Dog.sex.values.sample }
    document { "BCU #{Faker::Number.decimal(l_digits: 3, r_digits: 6)}" }
    date_of_birth { Date.today - 2.years }
    breeder { Faker::Name.name }
    owner { Faker::Name.name }
    mother_name { Faker::Artist.name }
    father_name { Faker::Artist.name }
    color_id { :white }
    doc_img { Rack::Test::UploadedFile.new('spec/support/pedigree.jpg') }
  end
end
