# frozen_string_literal: true

require 'rails_helper'

describe Registrations::DetectPrice do
  subject { described_class.call registration }

  let(:event) { build_stubbed :event, prices: prices }
  let(:prices) do
    {
      '2022-04-13' => {
        'baby' => { 'by' => { 'value' => '30', 'currency' => 'BYN' } },
        'puppy' => { 'by' => { 'value' => '30', 'currency' => 'BYN' } },
        'junior' => { 'by' => { 'value' => '35', 'currency' => 'BYN' } },
        'intermediate' => { 'by' => { 'value' => '35', 'currency' => 'BYN' } },
        'open' => { 'by' => { 'value' => '35', 'currency' => 'BYN' } },
        'winner' => { 'by' => { 'value' => '35', 'currency' => 'BYN' } },
        'work' => { 'by' => { 'value' => '35', 'currency' => 'BYN' } },
        'champ' => { 'by' => { 'value' => '35', 'currency' => 'BYN' } },
        'veteran' => { 'by' => { 'value' => '30', 'currency' => 'BYN' } },
        'detection' => { 'by' => { 'value' => '35', 'currency' => 'BYN' } }
      },
      '2022-04-24' => {
        'baby' => { 'by' => { 'value' => '35', 'currency' => 'BYN' } },
        'puppy' => { 'by' => { 'value' => '35', 'currency' => 'BYN' } },
        'junior' => { 'by' => { 'value' => '40', 'currency' => 'BYN' } },
        'intermediate' => { 'by' => { 'value' => '40', 'currency' => 'BYN' } },
        'open' => { 'by' => { 'value' => '40', 'currency' => 'BYN' } },
        'winner' => { 'by' => { 'value' => '40', 'currency' => 'BYN' } },
        'work' => { 'by' => { 'value' => '40', 'currency' => 'BYN' } },
        'champ' => { 'by' => { 'value' => '40', 'currency' => 'BYN' } },
        'veteran' => { 'by' => { 'value' => '35', 'currency' => 'BYN' } },
        'detection' => { 'by' => { 'value' => '40', 'currency' => 'BYN' } }
      }
    }
  end
  let(:registration) do
    build_stubbed :registration,
                  event: event,
                  show_class: show_class,
                  created_at: created_at
  end

  context 'when created_at' do
    context 'less than 2022-04-13' do
      let(:created_at) { Date.parse('2022-04-13').midday }

      context 'when show_class' do
        context 'baby' do
          let(:show_class) { :baby }

          it { is_expected.to eq Money.new(30_00, :BYN) }
        end

        context 'puppy' do
          let(:show_class) { :puppy }

          it { is_expected.to eq Money.new(30_00, :BYN) }
        end

        context 'junior' do
          let(:show_class) { :junior }

          it { is_expected.to eq Money.new(35_00, :BYN) }
        end

        context 'intermediate' do
          let(:show_class) { :intermediate }

          it { is_expected.to eq Money.new(35_00, :BYN) }
        end

        context 'open' do
          let(:show_class) { :open }

          it { is_expected.to eq Money.new(35_00, :BYN) }
        end

        context 'winner' do
          let(:show_class) { :winner }

          it { is_expected.to eq Money.new(35_00, :BYN) }
        end

        context 'champ' do
          let(:show_class) { :champ }

          it { is_expected.to eq Money.new(35_00, :BYN) }
        end

        context 'veteran' do
          let(:show_class) { :veteran }

          it { is_expected.to eq Money.new(30_00, :BYN) }
        end
      end
    end

    context 'less than 2022-04-24' do
      let(:created_at) { Date.parse('2022-04-24').midday }

      context 'when show_class' do
        context 'baby' do
          let(:show_class) { :baby }

          it { is_expected.to eq Money.new(35_00, :BYN) }
        end

        context 'puppy' do
          let(:show_class) { :puppy }

          it { is_expected.to eq Money.new(35_00, :BYN) }
        end

        context 'junior' do
          let(:show_class) { :junior }

          it { is_expected.to eq Money.new(40_00, :BYN) }
        end

        context 'intermediate' do
          let(:show_class) { :intermediate }

          it { is_expected.to eq Money.new(40_00, :BYN) }
        end

        context 'open' do
          let(:show_class) { :open }

          it { is_expected.to eq Money.new(40_00, :BYN) }
        end

        context 'winner' do
          let(:show_class) { :winner }

          it { is_expected.to eq Money.new(40_00, :BYN) }
        end

        context 'champ' do
          let(:show_class) { :champ }

          it { is_expected.to eq Money.new(40_00, :BYN) }
        end

        context 'veteran' do
          let(:show_class) { :veteran }

          it { is_expected.to eq Money.new(35_00, :BYN) }
        end
      end
    end

    context 'more than 2022-04-24' do
      let(:created_at) { Date.parse('2022-04-25').midday }

      context 'when show_class' do
        context 'baby' do
          let(:show_class) { :baby }

          it { is_expected.to eq Money.new(35_00, :BYN) }
        end

        context 'puppy' do
          let(:show_class) { :puppy }

          it { is_expected.to eq Money.new(35_00, :BYN) }
        end

        context 'junior' do
          let(:show_class) { :junior }

          it { is_expected.to eq Money.new(40_00, :BYN) }
        end

        context 'intermediate' do
          let(:show_class) { :intermediate }

          it { is_expected.to eq Money.new(40_00, :BYN) }
        end

        context 'open' do
          let(:show_class) { :open }

          it { is_expected.to eq Money.new(40_00, :BYN) }
        end

        context 'winner' do
          let(:show_class) { :winner }

          it { is_expected.to eq Money.new(40_00, :BYN) }
        end

        context 'champ' do
          let(:show_class) { :champ }

          it { is_expected.to eq Money.new(40_00, :BYN) }
        end

        context 'veteran' do
          let(:show_class) { :veteran }

          it { is_expected.to eq Money.new(35_00, :BYN) }
        end
      end
    end

    pending 'nil'
  end
end
