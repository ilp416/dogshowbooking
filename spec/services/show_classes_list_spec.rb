# frozen_string_literal: true

require 'rails_helper'

describe ShowClassesList do
  subject { described_class.call event }

  let(:event) { build :event, rank: rank, subject: subj_str }
  let(:std_classes) { ShowClassesList::STD_CLASSES }
  let(:german_shepherd_classes) { ShowClassesList::GERMAN_SHEPHERD_DOG_CLASSES }
  let(:subj_str) { ':border_collie,' }

  context 'when event rank' do
    context 'universal' do
      let(:rank) { :universal }

      it { is_expected.to eq std_classes }
    end

    context 'mono' do
      let(:rank) { :mono }

      it { is_expected.to eq std_classes }
    end

    context 'champ' do
      let(:rank) { :champ }

      context 'when subject' do
        context 'eq german_shepherd champ' do
          let(:subj_str) { ':german_shepherd_dog, :german_shepherd_dog_long,' }

          it { is_expected.to eq german_shepherd_classes }
        end

        context 'other' do
          it { is_expected.to eq std_classes }
        end
      end
    end
  end
end
