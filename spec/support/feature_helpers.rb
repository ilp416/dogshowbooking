# frozen_string_literal: true

module FeatureHelpers
  def sign_in_as(user)
    visit root_path
    find('input[name="user[email]"]').set(user.email)
    find('input[name="user[password]"]').set(user.password)
    click_on I18n.t('devise.login')
    expect(page).to have_text I18n.t('devise.sessions.signed_in')
  end
end
