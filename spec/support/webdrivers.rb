# frozen_string_literal: true

require 'capybara/rspec'
# require 'chromedriver/helper'

Capybara.register_driver :headless_chrome do |app|
  capabilities = Selenium::WebDriver::Remote::Capabilities.chrome(
    chromeOptions: { args: %w[headless disable-gpu window-size=1920,1080 start-maximized] }
  )

  options = {
    browser: :chrome,
    desired_capabilities: capabilities
  }

  Capybara::Selenium::Driver.new(app, options)
end

# Capybara.register_driver :chrome do |app|
#   capabilities = Selenium::WebDriver::Remote::Capabilities.chrome(
#     chromeOptions: { args: %w[window-size=1920,1080 start-maximized] }
#   )
# 
#   options = {
#     browser: :chrome,
#     desired_capabilities: capabilities
#   }
# 
#   Capybara::Selenium::Driver.new(app, options)
# end

# Capybara.asset_host = 'http://localhost:3001'
# Capybara.server = :puma
# Capybara.default_max_wait_time = 15
Capybara.javascript_driver = :headless_chrome
