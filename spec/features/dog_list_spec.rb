# frozen_string_literal: true

require 'rails_helper'

feature 'Dog list' do
  describe 'visit /dogs' do
    context 'when user not logged in' do
      scenario do
        visit dogs_path

        expect(current_path).to eq signin_path
        expect(page).to have_text I18n.t('sign_in')
      end
    end

    context 'when user logged in' do
      let!(:user) { create :user }
      before do
        sign_in_as user
      end

      context 'when user has no any dog' do
        scenario do
          visit dogs_path

          expect(current_path).to eq new_dog_path
        end
      end

      context 'when user has at less a dog' do
        let!(:dog) { create :dog, user: user }

        scenario do
          visit dogs_path

          expect(current_path).to eq dogs_path
          expect(page).to have_text I18n.t('dogs.index.title')
        end

        context 'when dog has registation' do
          let!(:registation) { create :registration, dog: dog, event: event }
          let(:event) { create :event, starts_on: event_date }

          before { visit dogs_path }

          context 'when event' do
            context 'was in past' do
              let(:event_date) { Time.zone.yesterday }

              it { expect(page).not_to have_text event.name }

              describe 'user can show or hide past registrations' do
                it do
                  click_link 'show_all'
                  expect(page).to have_text event.name
                  click_link 'show_active'
                  expect(page).not_to have_text event.name
                end
              end
            end

            context 'running today' do
              let(:event_date) { Time.zone.today }

              it { expect(page).to have_text event.name }
            end

            context 'is future' do
              let(:event_date) { Time.zone.tomorrow }

              it { expect(page).to have_text event.name }

              context 'when registration catalog_number' do
                let(:event) do
                  create :event,
                         starts_on: event_date,
                         catalog_published_at: catalog_date
                end

                context 'present' do
                  let!(:registation) do
                    create :registration,
                           dog: dog,
                           event: event,
                           catalog_number: catalog_number
                  end
                  let(:catalog_number) { 123 }
                  let(:now) { Time.zone.now }

                  context 'when event catalog_published_at' do
                    context 'in future' do
                      let(:catalog_date) { now + 5.minutes }

                      it { expect(page).not_to have_text catalog_number }
                    end

                    context 'in past' do
                      let(:catalog_date) { now - 5.minutes }

                      it { expect(page).to have_text catalog_number }
                    end
                  end
                end

                pending 'empty' do
                  context 'when event catalog_published_at' do
                    context 'in future' do
                      let(:catalog_date) { now + 5.minutes }
                    end

                    context 'in past' do
                      let(:catalog_date) { now - 5.minutes }
                    end
                  end
                end
              end
            end
          end
        end
      end
    end
  end
end
