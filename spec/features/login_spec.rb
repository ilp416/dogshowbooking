# frozen_string_literal: true

require 'rails_helper'

feature 'Loggining in', js: true, driver: :headless_chrome do
  before do
    OmniAuth.config.test_mode = true
    OmniAuth.config.add_mock(
      :google_oauth2,
      uid: '12345',
      info: { email: email }
    )
  end

  let(:email) { Faker::Internet.email }
  let(:new_user) { User.find_by email: email }
  let(:target_path) { '/' }

  def auth_with_google
    find("a[href='#{user_google_oauth2_omniauth_authorize_path}']").click
  end

  def input_required_user_fields
    find('input[name="user[first_name]"]').set(Faker::Name.first_name)
    find('input[name="user[last_name]"]').set(Faker::Name.last_name)
    find('input[name="user[phone]"]').set(Faker::PhoneNumber.phone_number)
    click_on 'Сохранить'
  end

  scenario 'User login through oauth first time' do
    visit target_path
    expect(current_path).to eq signin_path

    auth_with_google

    # It redirect to page to field in required user data
    expect(current_path).to eq update_profile_path
    input_required_user_fields

    expect(new_user.reload.valid?).to be_truthy
    expect(current_path).to eq target_path
    # TODO: check notice
  end

  context 'when users starts with complicated path' do
    let(:target_path) { '/users?show_all=1' }

    scenario 'User login through oauth first time' do
      visit target_path

      expect(current_path).to eq signin_path

      auth_with_google

      # It redirect to page to field in required user data
      expect(current_path).to eq update_profile_path
      input_required_user_fields

      expect(new_user.reload.valid?).to be_truthy
      expect(page).to have_current_path target_path
    end
  end

  context 'User has valid account' do
    let!(:user) { create :user, email: email }

    scenario 'User login through oauth' do
      expect(user.valid?).to be_truthy

      visit target_path

      expect(current_path).to eq signin_path

      auth_with_google

      expect(current_path).to eq target_path
    end
  end

  context 'invalid user skip some fields' do
    # TODO: When some fields skipped
    # TODO: show errors
    let!(:user) do
      user = build :user, email: email, last_name: nil
      user.save validate: false
      user
    end

    scenario 'User login through oauth' do
      expect(user.valid?).to be_falsey

      visit target_path
      expect(current_path).to eq signin_path
      auth_with_google

      expect(current_path).to eq update_profile_path
      click_on 'Сохранить'
      expect(current_path).to eq update_profile_path

      input_required_user_fields
      expect(current_path).to eq target_path
    end
  end

  pending 'Error auth'
end
