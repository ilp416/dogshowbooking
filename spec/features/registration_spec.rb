# frozen_string_literal: true
# As a new user I must see sign up form
# After registration I got a page to add a new dog
# On the New Dog page I should see available events
# After finish adding Dog I should get 'root' page
