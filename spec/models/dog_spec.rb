# frozen_string_literal: true

describe Dog do
  describe 'uniqueness' do
    subject { new_dog.valid? }

    let!(:dog) { create :dog, build(:dog).attributes.merge(dog_params) }
    let(:new_dog) { Dog.new build(:dog).attributes.merge(dog_params) }

    context 'name, breed and date of birth combination' do
      let(:dog_params) do
        { name: 'Merlin', breed_id: 'chinese_crested_dog', date_of_birth: Date.today - 1.year }
      end

      let(:message) { 'Собака уже есть в системе' }

      it 'must be uniq' do
        is_expected.to be_falsey
        expect(new_dog.errors[:name]).to include message
      end
    end
  end

  describe 'age' do
    subject { dog.age }

    let(:dog) { build :dog, date_of_birth: date }

    context '3 months' do
      let(:date) { Date.today - 3.month }

      it { is_expected.to eq '3 месяца' }
    end

    context '6 months' do
      let(:date) { Date.today - 6.month }

      it { is_expected.to eq '6 месяцев' }
    end

    context '23 months' do
      let(:date) { Date.today - 23.month }

      it { is_expected.to eq '23 месяца' }
    end

    context '24 months' do
      let(:date) { Date.today - 24.month }

      it { is_expected.to eq '2 года' }
    end
  end

  describe 'color_str' do
    let(:dog) do
      dog = build :dog, color_id: color_id, color_str: input_value
      dog.save
      dog
    end

    context 'when color_id' do
      context 'is nil' do
        let(:color_id) { nil }

        describe ' БеЛый ' do
          let(:input_value) { ' БеЛый ' }

          it do
            expect(dog.color_str).to be_nil
            expect(dog.color_id).to eq 'white'
          end
        end

        pending ' БеЛый, ' do
          let(:input_value) { ' БеЛый, ' }

          it do
            expect(dog.color_str).to be_nil
            expect(dog.color_id).to eq 'white'
          end
        end

        describe 'цвет КОТОРОГО нет' do
          let(:input_value) { 'цвет КОТОРОГО нет' }

          it do
            expect(dog.color_str).to eq input_value.mb_chars.downcase.to_s
            expect(dog.color_id).to be_nil
          end
        end
      end

      context 'any value' do
        let(:color_id) { :red }

        describe ' БеЛый ' do
          let(:input_value) { ' БеЛый ' }

          it do
            dog.reload
            expect(dog.color_str).to be_nil
            expect(dog.color_id).to eq 'red'
          end
        end
      end
    end
  end

  describe '.human_color_group' do
    subject { dog.human_color_group }

    let(:dog) { build :dog, breed_id: breed_id, color_id: color_id }

    context 'breed_id' do
      context 'pomeranian' do
        let(:breed_id) { :pomeranian }

        context 'color_id' do
          context 'brown' do
            let(:color_id) { :brown }

            it { is_expected.to eq I18n.t('color_groups.black_and_brown') }
          end

          context 'sable' do
            let(:color_id) { :sable }

            it { is_expected.to eq I18n.t('color_groups.orange_cream_sable') }
          end
        end
      end

      context 'dobermann' do
        let(:breed_id) { :dobermann }

        context 'black_rust' do
          context 'brown' do
            let(:color_id) { :black_rust }

            it { is_expected.to eq I18n.t('colors.black_rust') }
          end
        end
      end
    end
  end

  describe '.document_is_pedigree?' do
    subject { dog.document_is_pedigree? }

    let(:dog) { build :dog, document: doc_str }

    context 'when document' do
      context 'exchange' do
        let(:doc_str) { :exchange }

        it { is_expected.to be_falsey }
      end

      context 'puppy_card' do
        let(:doc_str) { :exchange }

        it { is_expected.to be_falsey }
      end

      context 'value' do
        let(:doc_str) { '1234531' }

        it { is_expected.to be_truthy }
      end

      context 'empty' do
        let(:doc_str) { '' }

        it { is_expected.to be_falsey }
      end
    end
  end
end
