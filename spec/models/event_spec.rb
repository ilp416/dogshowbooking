# frozen_string_literal: true

describe Event do
  describe '.root_event' do
    let!(:club1) { create :club }
    let!(:club2) { create :club }
    let!(:event01) { create :event, club: club1 }
    let!(:event02) { create :event, club: club1 }
    let!(:event11) { create :event, club: club2 }
    let!(:event12) { create :event, club: club2 }
    let!(:event13) { create :event, club: club2 }

    it {
      expect(event02.root_event).to eq event01
      expect(event13.root_event).to eq event11
    }
  end

  describe '#available_for_dog' do
    context 'event rank & subject' do
      subject { described_class.available_for_dog dog }

      let(:dog) { build :dog }

      context 'all_breeds' do
        let!(:event) { create :event, rank: :universal }

        it { is_expected.to include event }
      end

      context 'mono group' do
        let!(:event) { create :event, rank: :mono, subject: 'group9' }

        it { is_expected.to include event }

        context 'when breed in group 11 and has alt_dog attr' do
          let(:dog) { build :dog, breed_id: :beaver_terrier }

          it { is_expected.to include event }
        end
      end

      context 'mono single breed' do
        let!(:event) { create :event, rank: :mono, subject: ':chinese_crested_dog,' }

        it { is_expected.to include event }

        context 'when breed name is ambiguous' do
          let!(:event) do
            create :event,
                   rank: :mono,
                   subject: ':german_shepherd_dog_long,'
          end

          let(:dog) { build :dog, breed_id: :german_shepherd_dog }

          it { is_expected.not_to include event }
        end
      end

      context 'mono few breeds' do
        let!(:event) do
          create :event,
                 rank: :mono,
                 subject: ':poodle_standard, :poodle_medium_size,'
        end

        it { is_expected.not_to include event }

        context 'when dog has matched breed' do
          let(:dog) { build :dog, breed_id: :poodle_standard }

          it { is_expected.to include event }
        end
      end
    end
  end

  describe '#opened' do
    subject { described_class.opened }

    let!(:event) do
      create :event, registration_closed_at: registration_closed_at
    end

    context 'when registration_closed_at' do
      context 'future' do
        let(:registration_closed_at) { Time.zone.now + 2.days }

        it { is_expected.to include event }
      end

      context 'past' do
        let(:registration_closed_at) { Time.zone.now - 1.minute }

        it { is_expected.not_to include event }
      end
    end
  end
end
