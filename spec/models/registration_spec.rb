# frozen_string_literal: true

describe Dog do
  describe 'catalog_number' do
    let(:event) { create :event }
    subject { create :registration, event: event }

    it { expect(subject.catalog_number).to be_nil }

    context 'when other registration for same event' do
      context 'has catalog_number' do
        before { create :registration, event: event, catalog_number: 273 }

        it { expect(subject.catalog_number).to eq 274 }
      end
    end
  end
end
