# frozen_string_literal: true

describe User do
  subject { user }

  describe 'phone' do
    subject do
      user = User.create phone: phone
      user.errors[:phone]
    end

    let(:user) { create :user }

    let(:wrong_number_error) do
      I18n.t 'activerecord.errors.models.user.attributes.phone.wrong_number'
    end
    let(:blank_error) { I18n.t 'errors.messages.blank' }

    context 'when phone' do
      context 'ok' do
        let(:phone) { '+375 (29) 555-55-55' }

        it { is_expected.to be_empty }
      end

      context 'blank string' do
        let(:phone) { '' }

        it { is_expected.to include wrong_number_error }
      end

      context 'nil' do
        let(:phone) { nil }

        it { is_expected.to include wrong_number_error }
      end

      context 'wrong number' do
        let(:phone) { '+375 (29) a55-55' }

        it { is_expected.to include wrong_number_error }
      end
    end
  end

  describe '.adminable_clubs' do
    subject { user.adminable_clubs }

    let(:user) { create :user }
    let(:club) { create :club }
    let!(:role) { create :role, user: user, roleable: club }

    it { is_expected.to eq [club] }
  end

  describe '.roleable_events' do
    subject { user.roleable_events }

    let(:user) { create :user }
    let!(:event1) { create :event }
    let!(:event2) { create :event }
    let!(:event3) { create :event }
    let!(:role1) { create :role, user: user, roleable: event1 }
    let!(:role3) { create :role, user: user, roleable: event3 }

    it { is_expected.to eq [event1, event3] }
  end

  describe '.adminable_events' do
    subject { user.adminable_events }

    let(:user) { create :user }
    let(:club) { create :club }
    let!(:event1) { create :event }
    let!(:event2) { create :event }
    let!(:event3) { create :event, club: club }
    let!(:role) { create :role, user: user, roleable: club }
    let!(:role1) { create :role, user: user, roleable: event1 }
    let!(:role2) { create :role, user: user, roleable: event2 }

    it { is_expected.to eq [event1, event2, event3] }

    context 'without privileges' do
      subject { user2.adminable_events }

      let(:user2) { create :user }

      it { is_expected.to be_empty }
    end
  end
end
