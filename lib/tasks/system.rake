# frozen_string_literal: true

namespace :system do
  desc 'Assets precompile'
  task precompile: :environment do
    next unless Rails.env.production?

    `cd /home/deploy/www/mydog.show/current`
    `RAILS_ENV=production bundle exec rake assets:precompile`
  end
end
