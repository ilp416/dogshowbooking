# frozen_string_literal: true

module CoreExtensions
  module String
    module Random
      def random(length)
        rand(2**256).to_s(36)[0..length]
      end
    end
  end
end
