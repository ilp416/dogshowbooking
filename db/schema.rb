# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2022_07_26_062000) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "active_storage_attachments", force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.bigint "record_id", null: false
    t.bigint "blob_id", null: false
    t.datetime "created_at", null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.bigint "byte_size", null: false
    t.string "checksum", null: false
    t.datetime "created_at", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "clubs", force: :cascade do |t|
    t.string "name"
    t.string "fullname"
    t.string "slug"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.text "payment_requisites"
    t.text "phones"
  end

  create_table "dogs", force: :cascade do |t|
    t.string "breed_id"
    t.string "breed_str"
    t.string "name"
    t.string "color_str"
    t.string "sex"
    t.string "document"
    t.date "date_of_birth"
    t.string "breeder"
    t.string "owner"
    t.string "mother_name"
    t.string "father_name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "user_id"
    t.string "color_id"
    t.string "hair_type_id"
    t.datetime "deleted_at"
    t.text "titles"
    t.string "chip"
    t.string "stigma"
    t.index ["user_id"], name: "index_dogs_on_user_id"
  end

  create_table "events", force: :cascade do |t|
    t.string "name"
    t.string "short_name"
    t.date "starts_on"
    t.string "rank"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "subject"
    t.bigint "club_id", null: false
    t.datetime "catalog_published_at"
    t.boolean "catalog_hidden_for_all", default: false
    t.boolean "catalog_hidden_not_registered", default: false
    t.jsonb "prices"
    t.datetime "registration_closed_at"
    t.text "address"
    t.string "location"
    t.integer "catalog_status"
    t.index ["club_id"], name: "index_events_on_club_id"
  end

  create_table "payments", force: :cascade do |t|
    t.bigint "club_id"
    t.date "events_starts_on"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "user_id"
    t.index ["club_id"], name: "index_payments_on_club_id"
    t.index ["user_id"], name: "index_payments_on_user_id"
  end

  create_table "registrations", force: :cascade do |t|
    t.string "show_class"
    t.bigint "dog_id", null: false
    t.bigint "event_id", null: false
    t.bigint "user_id"
    t.text "note"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.boolean "paper_wanted", default: false
    t.text "admin_note"
    t.datetime "paid_at"
    t.integer "price_cents", default: 0, null: false
    t.string "price_currency", default: "BYN", null: false
    t.datetime "deleted_at"
    t.integer "catalog_number"
    t.datetime "paper_gived_out_at"
    t.bigint "payment_id"
    t.boolean "additional_list"
    t.index ["dog_id"], name: "index_registrations_on_dog_id"
    t.index ["event_id"], name: "index_registrations_on_event_id"
    t.index ["payment_id"], name: "index_registrations_on_payment_id"
    t.index ["user_id"], name: "index_registrations_on_user_id"
  end

  create_table "roles", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.string "role"
    t.string "roleable_type"
    t.bigint "roleable_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["roleable_type", "roleable_id"], name: "index_roles_on_roleable_type_and_roleable_id"
    t.index ["user_id"], name: "index_roles_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "first_name"
    t.string "second_name"
    t.string "last_name"
    t.string "phone"
    t.string "role"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.string "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string "unconfirmed_email"
    t.integer "failed_attempts", default: 0, null: false
    t.string "unlock_token"
    t.datetime "locked_at"
    t.string "name"
    t.string "avatar"
    t.index ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
    t.index ["unlock_token"], name: "index_users_on_unlock_token", unique: true
  end

  add_foreign_key "active_storage_attachments", "active_storage_blobs", column: "blob_id"
  add_foreign_key "dogs", "users"
  add_foreign_key "events", "clubs"
  add_foreign_key "payments", "users"
  add_foreign_key "registrations", "dogs"
  add_foreign_key "registrations", "events"
  add_foreign_key "registrations", "users"
  add_foreign_key "roles", "users"
end
