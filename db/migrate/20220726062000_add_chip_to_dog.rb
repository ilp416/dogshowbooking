class AddChipToDog < ActiveRecord::Migration[6.0]
  def change
    add_column :dogs, :chip, :string
    add_column :dogs, :stigma, :string
  end
end
