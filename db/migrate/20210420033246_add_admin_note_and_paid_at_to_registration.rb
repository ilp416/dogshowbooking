# frozen_string_literal: true

class AddAdminNoteAndPaidAtToRegistration < ActiveRecord::Migration[6.0]
  def change
    add_column :registrations, :admin_note, :text
    add_column :registrations, :paid_at, :datetime
  end
end
