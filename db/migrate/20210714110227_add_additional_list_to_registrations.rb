class AddAdditionalListToRegistrations < ActiveRecord::Migration[6.0]
  def change
    add_column :registrations, :additional_list, :boolean
  end
end
