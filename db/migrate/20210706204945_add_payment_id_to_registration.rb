class AddPaymentIdToRegistration < ActiveRecord::Migration[6.0]
  def change
    add_reference :registrations, :payment, index: true
  end
end
