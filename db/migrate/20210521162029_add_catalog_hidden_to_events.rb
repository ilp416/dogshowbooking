class AddCatalogHiddenToEvents < ActiveRecord::Migration[6.0]
  def change
    add_column :events, :catalog_hidden_for_all, :boolean, default: false
    add_column :events, :catalog_hidden_not_registered, :boolean, default: false
  end
end
