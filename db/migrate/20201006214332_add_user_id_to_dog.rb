# frozen_string_literal: true

class AddUserIdToDog < ActiveRecord::Migration[6.0]
  def change
    add_reference :dogs, :user, null: true, foreign_key: true
  end
end
