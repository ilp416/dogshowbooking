# frozen_string_literal: true

class AddBreedIdColorIdHairTypeIdToDogs < ActiveRecord::Migration[6.0]
  def change
    rename_column :dogs, :breed, :breed_id
    rename_column :dogs, :color, :color_str
    add_column :dogs, :color_id, :string
    add_column :dogs, :hair_type_id, :string
  end
end
