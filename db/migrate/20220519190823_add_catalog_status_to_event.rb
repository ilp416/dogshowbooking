class AddCatalogStatusToEvent < ActiveRecord::Migration[6.0]
  def up
    add_column :events, :catalog_status, :integer
    Event.all.find_each do |event|
      status = event.printable_catalog.attached? ? :done : :blank
      event.update catalog_status: status
    end
  end

  def down
    remove_column :events, :catalog_status
  end
end
