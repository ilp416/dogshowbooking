class AddPaperGivedOutAtToRegistration < ActiveRecord::Migration[6.0]
  def change
    add_column :registrations, :paper_gived_out_at, :datetime
  end
end
