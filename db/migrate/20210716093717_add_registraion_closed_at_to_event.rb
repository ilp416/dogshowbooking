class AddRegistraionClosedAtToEvent < ActiveRecord::Migration[6.0]
  def change
    add_column :events, :registration_closed_at, :datetime
  end
end
