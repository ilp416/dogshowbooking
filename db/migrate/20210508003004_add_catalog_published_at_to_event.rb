class AddCatalogPublishedAtToEvent < ActiveRecord::Migration[6.0]
  def change
    add_column :events, :catalog_published_at, :datetime
  end
end
