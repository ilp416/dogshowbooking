# frozen_string_literal: true

class CreateDogs < ActiveRecord::Migration[6.0]
  def change
    create_table :dogs do |t|
      t.string :breed
      t.string :breed_str
      t.string :name
      t.string :color
      t.string :sex
      t.string :document
      t.date :date_of_birth
      t.string :breeder
      t.string :owner
      t.string :mother_name
      t.string :father_name

      t.timestamps
    end
  end
end
