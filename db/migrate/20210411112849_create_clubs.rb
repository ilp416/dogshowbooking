# frozen_string_literal: true

class CreateClubs < ActiveRecord::Migration[6.0]
  def change
    create_table :clubs do |t|
      t.string :name
      t.string :fullname
      t.string :slug

      t.timestamps
    end
  end
end
