# frozen_string_literal: true

class FixNotNullUserIdForEvent < ActiveRecord::Migration[6.0]
  def change
    change_column_null :registrations, :user_id, true
  end
end
