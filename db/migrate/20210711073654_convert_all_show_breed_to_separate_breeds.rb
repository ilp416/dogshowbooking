class ConvertAllShowBreedToSeparateBreeds < ActiveRecord::Migration[6.0]
  def change
    Dog.where(breed_id: :great_dane, color_id: :brindle)
      .update_all breed_id: :great_dane_tan_and_brindle
    Dog.where(breed_id: :great_dane, color_id: :fawn)
      .update_all breed_id: :great_dane_tan_and_brindle, color_id: :tan

    Dog.where(breed_id: :miniature_schnauzer, color_id: :pepper_and_salt)
      .update_all breed_id: :miniature_schnauzer_pepper_and_salt
    Dog.where(breed_id: :miniature_schnauzer, color_id: :pure_black)
      .update_all breed_id: :miniature_schnauzer_pure_black
    Dog.where(breed_id: :miniature_schnauzer, color_id: :black_and_silver)
      .update_all breed_id: :miniature_schnauzer_black_and_silver
    Dog.where(breed_id: :miniature_schnauzer, color_id: :pure_white)
      .update_all breed_id: :miniature_schnauzer_pure_white

    Dog.where(breed_id: :schnauzer, color_id: :pepper_and_salt)
      .update_all breed_id: :schnauzer_pepper_and_salt
    Dog.where(breed_id: :schnauzer, color_id: :pure_black)
      .update_all breed_id: :schnauzer_pure_black

    Dog.where(breed_id: :miniature_size_spitz, color_id: :other_colours)
      .update_all breed_id: :miniature_size_spitz_orange_grey_other
    Dog.where(breed_id: :miniature_size_spitz, color_id: :orange)
      .update_all breed_id: :miniature_size_spitz_orange_grey_other

    Dog.where(breed_id: :weimaraner, hair_type_id: :long_haired)
      .update_all breed_id: :weimaraner_long_haired
    Dog.where(breed_id: :weimaraner, hair_type_id: :short_haired)
      .update_all breed_id: :weimaraner_short_haired

    Dog.where(breed_id: :russian_toy, hair_type_id: :long_haired)
      .update_all breed_id: :russian_toy_long_haired
    Dog.where(breed_id: :russian_toy, hair_type_id: :smooth_haired)
      .update_all breed_id: :russian_toy_smooth_haired

    Dog.where(breed_id: :poodle_standard, color_id: %i[brown black white])
      .update_all breed_id: :poodle_standard_classic
    Dog.where(breed_id: :poodle_standard, color_id: %i[silver apricot])
      .update_all breed_id: :poodle_standard_modern

    Dog.where(breed_id: :poodle_medium_size, color_id: %i[brown black white])
      .update_all breed_id: :poodle_medium_size_classic
    Dog.where(breed_id: :poodle_medium_size, color_id: %i[silver apricot])
      .update_all breed_id: :poodle_medium_size_modern

    Dog.where(breed_id: :poodle_miniature, color_id: %i[brown black white])
      .update_all breed_id: :poodle_miniature_classic
    Dog.where(breed_id: :poodle_miniature, color_id: %i[silver apricot])
      .update_all breed_id: :poodle_miniature_modern
  end
end
