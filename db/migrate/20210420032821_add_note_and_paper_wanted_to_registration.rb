# frozen_string_literal: true

class AddNoteAndPaperWantedToRegistration < ActiveRecord::Migration[6.0]
  def change
    add_column :registrations, :paper_wanted, :boolean, default: false
  end
end
