class AddPricesToEvent < ActiveRecord::Migration[6.0]
  def change
    add_column :events, :prices, :jsonb
  end
end
