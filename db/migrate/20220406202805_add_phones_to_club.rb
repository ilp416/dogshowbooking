class AddPhonesToClub < ActiveRecord::Migration[6.0]
  def change
    add_column :clubs, :phones, :text
  end
end
