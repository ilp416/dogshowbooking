class AddCatalogNumberToRegistration < ActiveRecord::Migration[6.0]
  def change
    add_column :registrations, :catalog_number, :integer
  end
end
