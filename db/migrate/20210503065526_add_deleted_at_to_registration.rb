class AddDeletedAtToRegistration < ActiveRecord::Migration[6.0]
  def change
    add_column :registrations, :deleted_at, :datetime,
      default: nil, null: true
  end
end
