# frozen_string_literal: true

class PhoneNumberValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    return if Phoner::Phone.valid? value

    record.errors.add attribute, error_message
  end

  private

  def error_message
    options[:message] || default_message
  end

  def default_message
    I18n.t 'activerecord.errors.models.user.attributes.phone.wrong_number'
  end
end
