# frozen_string_literal: true

class RegistrationsForPaymentQuery < ServiceObjectBase
  pattr_initialize :user

  def perform
    user.registrations
      .joins(:event)
      .where('events.starts_on >= ?', Time.zone.today)
      .order('events.starts_on', :club_id, :dog_id, :event_id)
  end
end
