class ShowClassesList < ServiceObjectBase
  pattr_initialize :event

  STD_CLASSES = %i[
    baby puppy junior
    intermediate open winner work champ veteran
    detection
  ].freeze

  GERMAN_SHEPHERD_DOG_CLASSES = %i[
    baby puppy teen young middle working veteran
  ].freeze

  def perform
    return GERMAN_SHEPHERD_DOG_CLASSES if german_shepherd?
    
    STD_CLASSES
  end

  private

  def german_shepherd?
    return false if event.rank != 'champ'
    return false if event.subject != ':german_shepherd_dog, :german_shepherd_dog_long,'

    true
  end
end
