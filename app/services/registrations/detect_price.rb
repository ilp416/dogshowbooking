# frozen_string_literal: true

module Registrations
  class DetectPrice < ServiceObjectBase
    pattr_initialize :reg

    def perform
      return if prices.blank?

      price = price_for_country
      return if price.blank?

      Money.new(price['value'].to_i * 100, price['currency'])
    end

    private

    def price_for_country
      prices_for_show_class.try(:[], 'by')
    end

    def prices_for_show_class
      prices_for_date.try(:[], reg.show_class)
    end

    def prices_for_date
      res = prices.find { |date_key, _data| Date.parse(date_key) >= reg_date }
      res ||= prices.sort.last
      res&.last
    end

    def prices
      reg.event.prices
    end

    def reg_date
      (reg.created_at || Time.zone.now).to_date
    end
  end
end
