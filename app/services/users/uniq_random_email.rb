# frozen_string_literal: true

module Users
  class UniqRandomEmail < ServiceObjectBase
    def perform
      uniq_random_email
    end

    private

    def uniq_random_email
      loop do
        email = random_email
        break email unless User.exists? email: email
      end
    end

    def random_email
      "#{String.random 16}@mydog.show"
    end
  end
end
