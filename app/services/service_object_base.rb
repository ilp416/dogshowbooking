# frozen_string_literal: true

# frozen_string_literal: true

# Base class for callable services
# For use short Service.call(args) instead of Service.new(args).perform

class ServiceObjectBase
  def self.call(*args)
    new(*args).perform
  end
end
