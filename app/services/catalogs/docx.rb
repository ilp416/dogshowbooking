# frozen_string_literal: true

require "sablon"

module Catalogs
  class Docx < ServiceObjectBase
    include ActionView::Helpers::SanitizeHelper

    TEMPLATE = Rails.root
      .join('app', 'services', 'catalogs', 'template.docx')
      .freeze

    TEMPLATE_WITH_PRICES = Rails.root
      .join('app', 'services', 'catalogs', 'template_prices.docx')
      .freeze

    pattr_initialize :event, [start_number: 1, debug: false, prices: false]

    def perform
      catalog_blocked_error unless prices

      event.update catalog_status: :processing

      render_to_file

      event_catalog.attached? && event_catalog.destroy
      event_catalog.attach(io: File.open(file.path), filename: filename)
      file.unlink
      event.update catalog_status: :done
    end

    private

    def event_catalog
      prices ? event.printable_catalog_with_prices : event.printable_catalog
    end

    def file
      return @file if @file
      @file = Tempfile.new(filename)
      @file.close
      @file
    end

    def render_to_file
      template.render_to_file File.expand_path(file.path), context
    end

    def template_file
      File.expand_path(
        prices ? TEMPLATE_WITH_PRICES : TEMPLATE
      )
    end

    def template
      @template ||= Sablon.template(template_file)
    end

    def filename
      "catalog#{event.id}.docx"
    end

    def filepath
      "/tmp/#{filename}"
    end

    def catalog_blocked_error
      return if event.registrations.where.not(catalog_number: nil).empty?

      raise "Catalog for event #{event.id} is blocked"
    end

    def next_catalog_number(dog)
      @catalog_number ||= start_number - 1
      @catalog_number += 1
      unless prices
        Registration.where(event_id: event.id, dog_id: dog.id)
          .update catalog_number: @catalog_number
      end
      @catalog_number
    end

    def context
      @context ||=
        {
          title: sanitize(event.name),
          groups: event.universal? ? groups : without_groups
        }
    end

    def without_groups
      [{ title: '', breeds: breeds(nil) }]
    end

    def groups
      sorted_groups.map do |group_id|
        {
          title: t("groups.#{group_id}"),
          breeds: breeds(group_id)
        }
      end
    end

    def breeds(group_id)
      puts "GROUP: #{group_id}"

      wanted_breeds = group_id.present? ? grouped_breeds[group_id] : all_breeds
      wanted_breeds
        .sort_by { |breed| breed.to_s}
        .map do |breed|
          puts "BREED: #{breed.key}"

          breed_dogs = dogs_for_breed(breed)

          puts breed_dogs.count
          next if breed_dogs.empty?

          has_hair_types = breed.sections_by_hair_type?(event)
          has_colors = breed.sections_by_color?(event)
          {
            title: breed.to_s,
            show_hair_types: has_hair_types,
            hair_types: has_hair_types && hair_types(breed_dogs, breed),
            show_colors: !has_hair_types && has_colors,
            colors:  !has_hair_types && has_colors ? colors(breed_dogs, breed) : [],
            genders: (!has_colors && !has_hair_types) ? dogs_by_gender(breed_dogs, breed) : []
          }
        end.compact
    end

    def hair_types(dogs, breed)
      breed.catalog_hair_types_hash.map do |hair_type_group, values|
        puts "HAIR_TYPE: #{hair_type_group}"

        dogs_by_hair = dogs.where(hair_type_id: values)
        puts dogs_by_hair.count
        next if dogs_by_hair.empty?

        has_colors = breed.sections_by_color?(event)
        {
          title: t("hair_types.#{hair_type_group}").mb_chars.titleize.to_s,
          show_colors: has_colors,
          colors: has_colors ? colors(dogs_by_hair, breed) : [],
          genders: !has_colors ? dogs_by_gender(dogs_by_hair, breed) : []
        }
      end.compact.sort_by { |hair_type| hair_type[:title] }
    end

    def colors(dogs, breed)
      breed.catalog_colors_hash
        .sort_by { |key, value| human_color(key) }
        .map do |color_group_id, values|
          puts "COLOR: #{color_group_id}"

          dogs_by_color = dogs.where(color_id: values)
          puts dogs_by_color.count
          next if dogs_by_color.empty?

          {
            title: human_color(color_group_id),
            genders: dogs_by_gender(dogs_by_color, breed)
          }
        end.compact
    end


    def human_color(color_group_id)
      if I18n.exists? "color_groups.#{color_group_id}"
        t("color_groups.#{color_group_id}").mb_chars.titleize
      else
        t("colors.#{color_group_id}").mb_chars.titleize
      end.to_s
    end

    def dogs_by_gender(dogs, breed)
      dogs.available_sexes.map do |gender|
        puts "GENDER: #{gender}"

        dogs_by_sex = dogs.with_sex gender

        puts dogs_by_sex.count
        next if dogs_by_sex.empty?

        {
          title: t("plural_sex.#{gender}"),
          show_classes: show_classes(dogs_by_sex)
        }
      end.compact
    end

    def show_classes(dogs)
      list = Registration.show_class.values |
        dogs.group(:show_class).pluck(:show_class)

      list.map do |show_class|
        dogs_by_class = dogs.where(registrations: { show_class: show_class }).order :name
        next if dogs_by_class.empty?
        {
          title: human_showclass(show_class),
          dogs: dogs_by_class.map{ |dog| dog_data dog }
        }
      end.compact
    end

    def human_showclass(key)
      t 'catalogs.show_class.label',
        value: t("enumerize.registration.show_class.#{key}").downcase
    end

    def show_str(str, prefix: nil)
      res = str.present? && str.match?('[a-яА-Я a-zA-z0-9]') && str.strip
      return '' unless res

      prefix ? "#{prefix}: #{res}" : "#{res}"
    end

    def dog_data(dog)
      line1 = debug ? dog.registration.catalog_number : ''
      line1 += "#{dog.name.mb_chars.strip.titleize}"\
        "#{dog.color && dog.color.match?('[a-яА-Я a-zA-z]') && ", #{dog.color.strip}"}"\
        ", #{I18n.l dog.date_of_birth}"\
        "#{dog.document.present? && dog.document.match?('[a-яА-Я a-zA-z0-9]') && ", #{dog.human_document.strip}"}"\
        "#{show_str(dog.chip, prefix: ", #{Dog.human_attribute_name(:chip).downcase}")}"\
        "#{show_str(dog.stigma, prefix: ", #{Dog.human_attribute_name(:stigma).downcase}")}"

      line2 = if dog.father_name.present? && dog.mother_name.present?
       "(#{dog.father_name.strip} x #{dog.mother_name.strip}),"
      elsif dog.father_name.present? || dog.mother_name.present?
       "(#{dog.father_name.strip}#{dog.mother_name.strip}),"
      end

      line3 = ''
      line3 += " #{t 'catalogs.dog.breeder_title'}: #{dog.breeder.strip}," if dog.breeder.present?
      line3 += "  #{t 'catalogs.dog.owner_title'}: #{dog.owner.strip}" if dog.owner.present?
      reg = dog.registrations.find_by event_id: event.id, deleted_at: nil

      {
        catalog_number: reg.catalog_number || next_catalog_number(dog),
        line1: "#{line1}",
        line2: "#{line2}",
        line3: "#{line3}",
        price: reg.human_price,
        paid: reg.paid?
      }
    end

    def t(*args)
      I18n.t *args
    end

    def dogs_for_breed(breed)
      Dog.where(breed_id: breed.key)
        .joins(:registrations)
        .merge(event.registrations.exists.main_list)
    end

    def breed_ids
      @breed_ids ||= event.dogs.group(:breed_id).pluck(:breed_id)
    end

    def all_breeds
      @all_breeds ||= breed_ids.map { |breed_id| Breed.find(breed_id) }
    end

    def grouped_breeds
      @grouped_breeds ||= all_breeds.group_by { |breed| breed.group }
    end

    def sorted_groups
      grouped_breeds.keys.sort_by { |group_id| I18n.t("group_ids.#{group_id}") }
    end
  end
end
