# frozen_string_literal: true

module Catalogs
  class CatalogGenerator
    pattr_initialize :event, [:additional_list]

    def groups
      I18n.t('groups').keys &
        fetch_breeds(event_breed_keys).map(&:group).uniq.map(&:to_sym)
    end

    def sorted_exists_event_breeds
      fetch_breeds sort_breeds_by_t event_breed_keys
    end

    def sorted_exists_group_breeds(group)
      fetch_breeds sort_breeds_by_t exists_group_breed_keys group
    end

    def event_breed_keys
      @event_breed_keys ||= Dog.joins(:registrations)
        .merge(registrations_scope)
        .group(:breed_id)
        .pluck(:breed_id)
    end

    def exists_group_breed_keys(group)
      event_breed_keys & group_breeds(group)
    end

    def fetch_breeds(keys)
      keys.map { |key| Breed.find key }
    end

    def sort_breeds_by_t(list)
      list.sort_by do |breed_key|
        t("breeds.#{breed_key}").downcase
      end
    end

    def group_breeds(group)
      all_breeds[group.to_s]&.keys || []
    end

    def all_breeds
      @all_breeds ||=
        YAML.safe_load File.read Rails.root.join 'config/breeds.yml'
    end

    def dogs(breed)
      Dog.joins(:registrations)
        .includes(:registrations)
        .merge(registrations_scope)
        .where(breed_id: breed.to_sym)
    end

    def registrations_scope
      return @registrations_scope if @registrations_scope
      scope = Registration
        .exists
        .where(event_id: event.id)
        .where.not(show_class: [nil, ''])

      @registrations_scope = additional_list? ? scope.additional_list : scope
    end

    def t(*args)
      I18n.t(*args)
    end

    def additional_list?
      additional_list
    end
  end
end
