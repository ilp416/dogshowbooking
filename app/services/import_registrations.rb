# frozen_string_literal: true

class ImportRegistrations
  pattr_initialize :filename

  def call
    data.each do |row|
      user = create_user_or_find_by row
      dog = create_dog row, user
      create_registrations dog, user, row
    rescue StandardError => e
      binding.pry
    end
  end

  private

  def create_user_or_find_by(row)
    user = User.find_by(email: row[:email])
    return user unless user.blank?

    user = User.new(email: row[:email], name: row[:name], phone: "+#{row[:phone]}")
    user.save(validate: false)
    user
  rescue
    binding.pry
  end

  def data
    @data ||= SmarterCSV.process( filename, col_sep: ";" )
  end

  def create_registrations(dog, user, row)
    events(row).each do |event|
      Registration.create(
        show_class: detect_show_class(row),
        user: user,
        dog: dog,
        event: event,
        note: row[:note],
        created_at: created_time(row)
      )
    end
  rescue
    binding.pry
  end

  def created_time(row)
    return Time.now if row[:created_at].blank?

    Time.parse(row[:created_at])
  end

  def events(row)
    values = row[:events]
    values.gsub! 'Республиканская выставка всех пород «Памяти Светланы Канаевой»', '35'
    values.gsub! 'Республиканская выставка собак-компаньонов и тоев', '37'
    values.gsub! 'Республиканская выставка терьеров', '36'

    ids = row[:events].split '#'

    Event.where id: ids
  end

  def create_dog(row, user)
    dog = Dog.where("name ILIKE ?", "#{row[:dog_name]}").find_by(user_id: user.id)
    dog ||= Dog.new(
        breed_id: detect_breed(row[:breed]),
        breed_str: row[:breed],
        name: row[:dog_name],
        color_str: row[:color],
        sex: detect_sex(row),
        document: row[:document],
        date_of_birth: Date.parse(row[:date_of_birth]),
        breeder: row[:breeder],
        owner: row[:owner],
        father_name: row[:father_name],
        mother_name: row[:mother_name],
        user: user
      )
    binding.pry unless dog.save
    dog
  rescue
    binding.pry
  end

  def detect_breed(val)
    breed = I18n.t('breeds').key downcase(val)
    binding.pry if breed.blank?

    breed
  end

  def detect_sex(row)
    downcase(row[:sex]) == 'сука' ? 'female' : 'male'
  end

  def detect_show_class(row)
    case row[:show_class]
    when 'Беби от 3 до 6 месяцев', 'Бэби - собаки от 3 до 6 месяцев'
      :baby
    when 'Щенки от 6 до 9 месяцев'
      :puppy
    when 'Юниоров от 9 до 18 месяцев'
      :junior
    when 'Промежуточный (Интермедия) от 15 до 24 месяцев'
      :intermediate
    when 'Открытый от 15 месяцев взрослый класс'
      :open
    when 'победителей', 'Победителей от 15 месяцев ( если у собаки есть хотя бы 1 взрослый САС или ЮЧБ ЮЧП ЧП Чемпион страны)'
      :winner
    when 'Чемпионов от 15 месяцев ( Звание Чемпион)'
      :champ
    when 'класс ветеранов от 8 лет и выше.'
      :veteran
    else
      binding.pry
    end
  end

  def downcase(str)
    return if str.blank?

    str.mb_chars.downcase.to_s
  end
end
