# frozen_string_literal: true

class DogsController < ApplicationController
  before_action :set_dog,
                only: %i[show edit update destroy registrations register]
  before_action :check_user_has_a_dog, except: %i[new create update]
  respond_to :json, only: %i[create update]

  # GET /dogs
  def index
    @dogs = @current_user.dogs
    @registrations = fetch_registrations

    respond_to do |format|
      format.html
      format.json { render json: @dogs.to_json }
    end
  end

  def search
    filtered_dogs = Dog.where('name ILIKE ?', "#{params[:search]}%")
      .order("user_id = #{current_user.id} DESC, created_at DESC")
    @dogs = Dog.select('DISTINCT ON (name) *').from(filtered_dogs).limit 10

    render json: @dogs.to_json
  end

  # GET /dogs/new
  def new
    @dog = Dog.new
  end

  # POST /dogs
  def create
    @dog = @current_user.dogs.build(dog_params)

    render_json_dog_or_errors @dog.save
  end

  # PATCH/PUT /dogs/1
  def update
    render_json_dog_or_errors @dog.update(dog_params)
  end

  # DELETE /dogs/1
  def destroy
    @dog.destroy
    redirect_to dogs_url, notice: 'Dog was successfully destroyed.'
  end

  def registrations
    respond_to do |format|
      format.html do
        available_events_empty_notice
        render
      end
      format.json do
        @registrations = all_available_registations
        @clubs = all_available_events.map(&:club).uniq
        @events = all_available_events.uniq
      end
    end
  end

  def register
    @registration = @current_user.registrations.build(registration_params)
    if @registration.save
      render json: @registration.to_json, status: 201
    else
      render json: @registration.errors.to_json, status: 400
    end
  end

  private

  def fetch_registrations
    Registration.joins(:event)
      .merge(params[:show_all] ? Event.all : Event.not_past)
      .to_a
      .group_by(&:dog_id)
  end

  def render_json_dog_or_errors(result)
    if result
      render json: @dog.to_json
    else
      render status: 400, json: { errors: @dog.errors }
    end
  end

  def available_events_empty_notice
    return if available_events.any?

    flash[:error] = I18n.t('registrations.has_no_available')
  end

  def check_user_has_a_dog
    return if current_user.privileged?
    return if current_user.dogs.any?

    redirect_to new_dog_path
  end

  def registration_params
    params.require(:registration).permit(:show_class, :dog_id, :event_id, :user_id, :note, :paper_wanted)
  end

  def available_events
    Event.not_past.opened.available_for_dog @dog
  end

  def all_available_events
    available_events + @dog.events.not_past
  end

  def all_available_registations
    all_available_events
      .uniq
      .sort_by { |event| [event.starts_on, event.club_id] }
      .map { |event| fetch_registration event }
  end

  def fetch_registration(event)
    event.registrations.find_by(dog_id: @dog.id) ||
      event.registrations.build(dog: @dog, paper_wanted: nil)
  end

  # Use callbacks to share common setup or constraints between actions.
  def set_dog
    dogs = current_user.superadmin? ? Dog : current_user.dogs

    @dog = dogs.find(params[:id])
  end

  # Only allow a trusted parameter "white list" through.
  def dog_params
    params
      .require(:dog)
      .permit(:breed, :breed_id, :name, :color_str, :color_id, :document, :doc_img,
              :date_of_birth, :hair_type, :hair_type_id, :sex, :breeder,
              :owner, :mother_name, :father_name, :titles, :chip, :stigma)
      .except :breed, :color, :hair_type
  end
end
