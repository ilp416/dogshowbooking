# frozen_string_literal: true

class BreedsController < ApplicationController
  def autocomplete_title
    res = humanized_breeds.select do |key, value|
      value.downcase.match?(query) || key.match?(query)
    end
    render json: res
  end

  def autocomplete_colors
    res = humanized_colors.select do |key, value|
      value.downcase.match?(query) || key.match?(query)
    end
    render json: res
  end

  private

  def query
    params[:q].downcase
  end

  def humanized_breeds
    Rails.cache.fetch(:humanized_breeds) do
      I18n.available_locales.sum do |locale|
        I18n.with_locale(locale) do
          BREED_LIST.keys.map { |i| [i, t("breeds.#{i}").capitalize] }
        end
      end
    end
  end

  def humanized_colors
    I18n.t('colors').map { |key, value| [key, value.capitalize] }
  end
end
