# frozen_string_literal: true

class ApplicationController < ActionController::Base
  include Pundit

  rescue_from ActiveRecord::RecordNotFound, with: :record_not_found

  before_action :masquerade_user!
  before_action :check_user_signed_in
  before_action :check_user_valid
  before_action :configure_permitted_parameters, if: :devise_controller?

  def check_user_signed_in
    return if devise_session_actions?
    return if current_user

    store_target_url
    redirect_to signin_path
  end

  def check_user_valid
    return if devise_session_actions?
    return if current_user.valid?

    store_target_url
    redirect_to update_profile_path
  end

  def record_not_found
    render file: '/public/404.html'
  end

  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up) do |user_params|
      user_params.permit(
        :last_name, :first_name, :second_name, :email, :phone,
        :password, :password_confirmation
      )
    end
  end

  private

  def devise_session_actions?
    params[:controller].in? %w[
      devise/sessions devise/registrations devise/passwords devise/unlocks
    ]
  end

  def store_target_url
    session[:redirect_after_user_valid] ||= request.fullpath
  end

  def redirect_to_target_url
    return false if session[:redirect_after_user_valid].blank?

    redirect_to session.delete(:redirect_after_user_valid)
  end
end
