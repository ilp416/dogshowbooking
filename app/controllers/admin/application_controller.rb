# frozen_string_literal: true

module Admin
  class ApplicationController < ApplicationController
    before_action :check_user_priveleged

    private

    def check_user_priveleged
      return current_user&.superadmin?
      return if current_user.roles.any?

      redirect_to :root
    end
  end
end
