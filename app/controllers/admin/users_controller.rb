# frozen_string_literal: true

module Admin
  class UsersController < Admin::ApplicationController
    before_action :set_user, only: %i[show edit destroy]

    # GET /users
    def index
      params[:with_role] ||= true
      @users = User.includes(:roleable_clubs, :roleable_events)
      @users = @users.where.not(roles: { id: nil }) if params[:with_role] != 'false'
      @users = search @users
    end

    # GET /users/1
    def show; end

    # GET /users/new
    def new
      @user = User.new
    end

    # GET /users/1/edit
    def edit; end

    # POST /users
    def create
      @user = User.new(user_params)

      if @user.save
        redirect_to :root, notice: I18n.t('user.created')
      else
        render :new
      end
    end

    # PATCH/PUT /users/1
    def update
      if @user.update(user_params)
        flash[:notice] = I18n.t('user.updated')
        redirect_to_target_url
        redirect_to(:root) unless performed?
      else
        render :update_profile
      end
    end

    # DELETE /users/1
    def destroy
      @user.destroy
      redirect_to users_url, notice: 'User was successfully destroyed.'
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def user_params
      params.require(:user).permit(:first_name, :second_name, :last_name, :phone, :role, :email)
    end

    def search(scope)
      return scope if params[:search].blank?

      scope.where(
        '
          users.last_name ILIKE ?
          OR users.phone ILIKE ?
          OR users.email ILIKE ?
        ', "%#{params[:search]}%", "%#{params[:search]}%", "%#{params[:search]}%"
      )
    end

  end
end
