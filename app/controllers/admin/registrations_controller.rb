# frozen_string_literal: true

module Admin
  class RegistrationsController < Admin::ApplicationController
    before_action :set_registration, only: %i[show edit update destroy]

    # GET /registrations
    def index
      @events = events.order(starts_on: :DESC, club_id: :ASC, id: :ASC)
      @dog_registrations = Dog
        .includes(:user, :registrations)
        .joins(:user, registrations: :event)
        .merge(registrations_to_show)
        .sort_by do |dog|
          breed = dog.breed_obj
          next [] unless breed.key

          [
            I18n.t("group_ids.#{breed.group}"),
            breed.to_s,
            dog.male? ? 0 : 1,
            dog.name
          ]
        end
    end

    def payments
      @events = events.order(starts_on: :DESC, club_id: :ASC, id: :ASC)
      @dog_registrations = Dog
        .includes(:user, :registrations)
        .joins(:user, registrations: :event)
        .merge(registrations_to_show)
        .where(registrations: { deleted_at: nil })
        .sort_by do |dog|
          breed = dog.breed_obj
          next [] unless breed.key

          [
            I18n.t("group_ids.#{breed.group}"),
            breed.to_s,
            dog.male? ? 0 : 1,
            dog.name
          ]
        end
      render layout: 'min'
    end

    # PATCH/PUT /registrations/1
    def update
      if @registration.update(registration_params)
        respond_to do |format|
          format.html do
            flash[:notice] = I18n.t('registration.updated')
            render :edit
          end
          format.json { render :registration }
        end
      else
        respond_to do |format|
          format.json do
            render status: 400, json: { errors: @registration.errors }
          end
          format.html { render :edit }
        end
      end
    end

    # DELETE /registrations/1
    def destroy
      @registration.destroy

      head :ok
    end

    private

    delegate :adminable_event_ids, to: :current_user

    def set_registration
      @registration ||= Registration.where(event_id: adminable_event_ids)
                                    .find_by id: params[:id]
    end

    def registration_params
      params.require(:registration).permit(
        :show_class, :dog_id, :event_id, :user_id, :admin_note, :note,
        :price_cents, :price_currency, :paid, :deleted_at, :paper_gived_out
      )
    end

    def adminable_events
      if current_user.superadmin?
        Event.all
      else
        Event.where id: adminable_event_ids
      end
    end

    def events
      if params[:event_ids]
        adminable_events.where(id: params[:event_ids])
      elsif events_date
        adminable_events.where(starts_on: events_date)
      else
        adminable_events
      end
    end

    def events_date
      params[:date] || event_date_list
    end

    def event_date_list
      return @event_date_list if @event_date_list.present?

      event_date ||= (
        adminable_events.not_past.order(:starts_on).first ||
        adminable_events.order(:starts_on).last
      ).try :starts_on

      @event_date_list ||= [event_date, event_date + 1.day]
    end

    def registrations_to_show
      Registration.where(event_id: events.map(&:id))
    end
  end
end
