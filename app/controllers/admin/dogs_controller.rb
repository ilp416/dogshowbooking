# frozen_string_literal: true

module Admin
  class DogsController < Admin::ApplicationController
    before_action :set_dog, only: %i[show edit update destroy]

    # GET /dogs
    def index
      @dogs = Dog.all
      @dogs = @dogs.where('color_id IS NULL OR breed_id IS NULL') if params[:not_success]
      @dogs = @dogs.limit(20)

      respond_to do |format|
        format.html
        format.json { render json: @dogs.to_json }
      end
    end

    # GET /dogs/1
    def show; end

    # GET /dogs/new
    def new
      @dog = Dog.new
    end

    # GET /dogs/1/edit
    def edit; end

    # POST /dogs
    def create
      @dog = @current_user.dogs.build(dog_params)

      if @dog.save
        render json: @dog.to_json
      else
        render status: 400, json: { errors: @dog.errors }
      end
    end

    # PATCH/PUT /dogs/1
    def update
      if @dog.update(dog_params)
        render json: @dog.to_json
      else
        render status: 400, json: { errors: @dog.errors }
      end
    end

    # DELETE /dogs/1
    def destroy
      @dog.destroy

      head :ok
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_dog
      dogs = if current_user.superadmin?
          Dog
        else
          Dog.joins(:registrations)
            .where registrations: { event_id: current_user.adminable_event_ids }
        end
      @dog = dogs.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def dog_params
      params
        .require(:dog)
        .permit(:breed, :breed_id, :name, :color_str, :color_id, :document, :doc_img,
                :date_of_birth, :hair_type, :hair_type_id, :sex, :breeder,
                :owner, :mother_name, :father_name, :titles, :chip, :stigma)
        .except :breed, :color, :hair_type
    end
  end
end
