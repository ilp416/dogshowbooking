# frozen_string_literal: true

module Admin
  class CatalogsController < Admin::ApplicationController
    skip_before_action :check_user_signed_in, only: %i[cards diploms]
    skip_before_action :check_user_valid, only: %i[cards diploms]
    skip_before_action :check_user_priveleged, only: %i[cards diploms]

    before_action :main_or_additional_registrations, only: %i[cards diploms]

    def show
      @event = event
      respond_to do |format|
        format.html
        format.json do
          @registrations = fetch_sorted_registrations
        end
      end
    end

    def additional_list
      @event = event
      @gen = Catalogs::CatalogGenerator.new(@event, additional_list: additional_list?)
    end

    def diploms
      fetch_event_name

      @blank_count = params[:blank_count].to_i || 0
      @registrations = @registrations.to_a
      @blank_count.times { @registrations.push nil }

      render template_key, layout: false
    end

    def cards
      fetch_event_name

      render cards_tempate_key, layout: false
    end

    private

    def template_key
      keys = ["diploms#{event.id}", "diploms_#{event.club.slug}"]
      keys.find do |key|
        File.exists? Rails.root.join "app/views/admin/catalogs/#{key}.html.slim"
      end
    end

    def cards_tempate_key
      custom_key = "cards_#{event.club.slug}"
      template_exists?("admin/catalogs/#{custom_key}") ? custom_key : 'cards'
    end

    def main_or_additional_registrations
      @registrations = if params[:additional]
                         registrations.additional_list
                       else
                         registrations.main_list
                       end
    end

    def registrations
      @registrations ||= event.registrations.exists.order(:catalog_number).exists
    end

    def additional_list?
      params[:additional]&.downcase&.to_s == 'true'
    end

    def fetch_event_name
      @event_name = if event.id == 1
        "Республиканская&nbsp;выставка собак&nbsp;всех&nbsp;пород"
                    elsif event.id == 2
        "Республиканская выставка шпицев и&nbsp;их&nbsp;примитивных&nbsp;типов"
      else
        @event_name = event.name
      end
    end

    def fetch_sorted_registrations
      @registrations = registrations
        .includes(:dog)
        .sort_by do |reg|
          dog = reg.dog
          breed = dog.breed_obj
          [
            @event.universal? ? I18n.t("group_ids.#{breed.group}") : '',
            breed.to_s,
            breed.sections_by_hair_type?(event) ? dog.human_hair_type : '',
            breed.sections_by_color?(event) ? dog.human_color_group : '',
            dog.male? ? 0 : 1,
            Registration.show_class.values.index(reg.show_class),
            dog.name
          ]
        end
    end

    def event
      # @event ||= event_scope.find params[:event_id]
      @event ||= Event.find params[:event_id]
    end

    def event_scope
      scope = Event.all
      return scope if current_user.superadmin?

      Event.where(id: current_user.adminable_event_ids)
    end
  end
end
