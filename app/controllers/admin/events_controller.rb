# frozen_string_literal: true

module Admin
  class EventsController < Admin::ApplicationController
    before_action :set_event, only: %i[
      show edit update destroy printable_catalog drop_catalog
      close_registration reopen_registration
      publish_catalog hide_catalog
    ]

    # GET /events
    def index
      @events = events
        .order(starts_on: :DESC, club_id: :ASC, id: :ASC)
        .includes(:club)
    end

    # GET /events/1
    def show; end

    # GET /events/new
    def new
      @event = Event.new new_event_params
    end

    # GET /events/1/edit
    def edit; end

    # POST /events
    def create
      @event = Event.new(event_params)

      if @event.save
        if params[:copy]
          attrs = @event.attributes.except('id')
          redirect_to new_admin_event_path(event: attrs)
        else
          redirect_to admin_events_path, notice: 'Event was successfully created.'
        end
      else
        render :new
      end
    end

    # PATCH/PUT /events/1
    def update
      if @event.update(event_params)
        if params[:copy]
          attrs = @event.attributes.except('id')
          redirect_to new_admin_event_path(event: attrs)
        else
          render :edit
        end
      else
        render :edit
      end
    end

    # DELETE /events/1
    def destroy
      @event.destroy
      redirect_to events_url, notice: 'Event was successfully destroyed.'
    end

    def printable_catalog
      send_file @event.printable_catalog.path
    end

    def gen_catalog
      GenerateCatalogJob.perform_async(
        params[:id], params[:prices].present?
      )

      redirect_to admin_events_path
    end

    def drop_catalog
      Registration.where(event_id: params[:id]).update_all catalog_number: nil
      @event.update catalog_status: :missing

      redirect_to admin_events_path
    end

    def close_registration
      @event.update registration_closed_at: Time.zone.now

      redirect_to admin_events_path
    end

    def reopen_registration
      @event.update registration_closed_at: Time.zone.now + 1.day

      redirect_to admin_events_path
    end

    def publish_catalog
      @event.update(
        catalog_published_at: Time.zone.now,
        catalog_hidden_for_all: false
      )

      redirect_to admin_events_path
    end

    def hide_catalog
      @event.update(
        catalog_hidden_for_all: true
      )
      
      redirect_to admin_events_path
    end

    private

    def init_prices
      {
        (Date.today + 1.week).to_s => {
          baby: baby_prices,
          open: open_prices
        }
      }
    end

    def baby_prices
      { by: { value: 35, currency: :BYN }, www: { value: 35, currecy: :RUB } }
    end

    def open_prices
      { by: { value: 40, currency: :BYN }, www: { value: 40, currecy: :RUB } }
    end

    def events
      scope = Event.all
      return scope if current_user.superadmin?

      Event.where(id: current_user.adminable_event_ids)
    end

    # Use callbacks to share common setup or constraints between actions.
    def set_event
      @event = events.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def event_params
      params.require(:event).permit(
        :name, :short_name, :starts_on, :rank, :subject,
        :catalog_published_at, :registration_closed_at,
        :address, :location, :location_map, :club_id, prices: {}
      )
    end

    def opt_event_params
      params.fetch(:event).permit(
        :name, :short_name, :starts_on, :rank, :subject,
        :catalog_published_at, :registration_closed_at,
        :address, :location, :location_map, :club_id, prices: {}
      )
    end

    def new_event_params
      if params[:event]
        opt_event_params
      else
        { prices: Event.last&.prices }
      end
    end
  end
end
