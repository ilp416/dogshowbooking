class CatalogsController < PublicController

  def index
    @events = Event.order(starts_on: :DESC, id: :ASC)
  end

  def show
    @event = Event.find params[:id]
    @gen = cached_generator
    @search_array = search_array
    respond_to do |format|
      format.html
      format.pdf do
        render pdf: 'catalog',
               template: '/catalogs/show.html.slim',
               page_size: 'A4',
               encoding: 'UTF-8'
      end
    end
  end

  private

  def cached_generator
    Catalogs::CatalogGenerator.new(@event)
  end

  def search_array
    breeds_search_array + dogs_search_array
  end

  def dogs_search_array
    regs_for_search.map do |reg|
      dog = reg.dog
      {
        type: :reg,
        search_str: dog_search_str(reg),
        key: "reg#{reg.id}",
        object: {
          title: dog.name,
          breed: dog.breed_obj.to_s,
          age: dog.age,
          show_class: reg.show_class_text,
          sex: dog.sex,
          gender: dog.sex_text,
        }
      }
    end
  end

  def dog_search_str(reg)
    dog = reg.dog
    "#{dog.name.mb_chars.downcase} "\
    "#{dog.breed_obj.to_s.mb_chars.downcase} "\
    "#{dog.breeder.mb_chars.downcase} "\
    "#{dog.owner.mb_chars.downcase} "\
    "#{dog&.user&.first_name&.mb_chars&.downcase} "\
    "#{dog&.user&.last_name&.mb_chars&.downcase} "\
    "#{dog&.user&.phone&.downcase} "\
    "#{dog&.user&.email&.downcase} "\
    "#{reg.show_class_text&.mb_chars&.downcase}"
  end

  def breeds_search_array
    breeds.map do |breed|
      {
        type: :breed,
        search_str: breed.to_s.mb_chars.downcase.to_s,
        key: "breed#{breed.key}",
        object: {
          title: breed.to_s
        }
      }
    end
  end

  def regs_for_search
    Registration.where(event_id: @event.id).includes(dog: :user).exists
  end

  def breeds
    regs_for_search.group('dogs.breed_id')
      .pluck(:breed_id)
      .map { |key| Breed.find key }
      .sort_by { |breed| breed.to_s }
  end

end
