# frozen_string_literal: true

module Users
  class OmniauthCallbacksController < Devise::OmniauthCallbacksController
    skip_before_action :verify_authenticity_token, only: :facebook
    skip_before_action :check_user_signed_in, :check_user_valid

    def google_oauth2
      oauth_action_for 'Google'
    end

    def facebook
      oauth_action_for 'Facebook'
    end

    def vkontakte
      oauth_action_for 'VK'
    end

    def failure
      redirect_to root_path
    end

    private

    def oauth_action_for(provider)
      if user.persisted?
        sign_in user, event: :authentication
        prepare_flash provider

        check_user_valid

        redirect_to_target_url unless performed?
        redirect_to(dogs_path) unless performed?
      else
        set_session
        redirect_to new_user_registration_url, alert: user_error_messages
      end
    end

    def prepare_flash(kind)
      set_flash_message(:notice, :success, kind: kind) if is_navigational_format?
    end

    def set_session
      # Removing extra as it can overflow some session stores
      session['devise.google_data'] = request.env['omniauth.auth'].except('extra')
    end

    def user_error_messages
      user.errors.full_messages.join("\n")
    end

    def user
      @user ||= User.from_omniauth(request.env['omniauth.auth'])
    end
  end
end
