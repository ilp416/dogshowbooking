# frozen_string_literal: true

module Users
  class SessionsController < Devise::SessionsController
    skip_before_action :check_user_valid, :check_user_signed_in

    # POST /resource/sign_in
    def create
      email_or_phone_params

      super
    end

    private

    def email_or_phone_params
      return if User.find_by(email: params[:user][:email]).present?
      return if phone_user.blank?

      params[:user][:email] = phone_user.email
    end

    def email_user
      @email_user ||= User.find_by(email: params[:user][:email])
    end

    def phone_user
      @phone_user ||= User.find_by(phone: formatted_phone(params[:user][:email]))
    end

    def formatted_phone(value)
      Phoner::Phone.parse(value).to_s
    rescue StandardError
      value
    end
  end
end
