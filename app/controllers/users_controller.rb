# frozen_string_literal: true

class UsersController < ApplicationController
  before_action :set_user, only: %i[show edit destroy]
  before_action :set_user_for_update, only: %i[update]
  skip_before_action :check_user_valid, only: %i[edit update_profile update]

  # GET /users
  def index
    @users = User.all
  end

  # GET /users/1
  def show; end

  # GET /users/new
  def new
    @user = User.new
  end

  # GET /users/1/edit
  def edit; end

  # GET /users/1/update_profile
  def update_profile
    @user = current_user
  end

  # POST /users
  def create
    @user = User.new(user_params)

    if @user.save
      redirect_to :root, notice: I18n.t('user.created')
    else
      render :new
    end
  end

  # PATCH/PUT /users/1
  def update
    if @user.update(user_params)
      flash[:notice] = I18n.t('user.updated')
      redirect_to_target_url
      redirect_to(:root) unless performed?
    else
      # render(referer_update_profile? ? :update_profile : :edit)
      render :update_profile
    end
  end

  # DELETE /users/1
  def destroy
    @user.destroy
    redirect_to users_url, notice: 'User was successfully destroyed.'
  end

  def facebook_user_deletion
    signed_request = params['signed_request']
    data = parse_fb_signed_request(signed_request)
    user = User.find_by(email: data['user_id'])
    user.destroy
    data = {
      url: "#{ENV['APP_HOST_URL']}/deletion_status?id=#{user.id}",
      confirmation_code: "del_#{user.id}"
    }
    render json: data
  end

  def deletion_status
    res = if User.find_by(uid: params[:id]).any?
        'User successfully deleted'
      else
        'User not deleted'
      end

    render text: res
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_user
    @user = User.find(params[:id])
  end

  def referer_update_profile?
    request.referer =~ %r{/update_profile$}
  end

  def set_user_for_update
    @user = referer_update_profile? ? @current_user : set_user
  end

  # Only allow a trusted parameter "white list" through.
  def user_params
    params.require(:user).permit(:first_name, :second_name, :last_name, :phone, :role, :email)
  end

  def parse_fb_signed_request(signed_request)
    encoded_sig, payload = signed_request.split('.', 2)
    secret = ENV['FACEBOOK_SECRET_ID']

    # Decode the data
    decoded_sig = Base64.urlsafe_decode64(encoded_sig)
    data = JSON.load(Base64.urlsafe_decode64(payload))

    # Create the HMAC signature
    expected_sig = OpenSSL::HMAC.digest("SHA256", secret, payload)

    if decoded_sig != expected_sig
      puts 'Bad Signed JSON signature!'
      return nil
    else
      data
    end
  end
end
