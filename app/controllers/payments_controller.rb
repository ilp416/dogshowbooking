# frozen_string_literal: true

class PaymentsController < ApplicationController
  def index
    @registrations = ::RegistrationsForPaymentQuery.call current_user
  end

  def create
    @payment = current_user.payments.create(payment_params)

    render json: @payment.to_json
  end

  private

  def payment_params
    params
      .require(:payment)
      .permit(:club_id, :events_starts_on, :check_img, registration_ids: [])
  end

  def registrations
    user.registrations
  end
end
