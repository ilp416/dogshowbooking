class PublicController < ApplicationController
  skip_before_action :check_user_signed_in
  skip_before_action :check_user_valid
end
