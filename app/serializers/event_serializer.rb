class EventSerializer < ActiveModel::Serializer
  attributes :id, :name, :short_name, :starts_on
end
