class RegistrationSerializer < ActiveModel::Serializer
  attributes :id, :dog_id, :show_class, :event_id, :paid_at,
             :note, :paper_wanted, :admin_note, :event
end

