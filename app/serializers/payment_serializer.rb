class PaymentSerializer < ActiveModel::Serializer
  delegate :check_img, to: :object

  attributes :id, :club_id, :check_image, :events_starts_on, :registration_ids, :created_at

  def check_image
    return {} unless check_img.attached?

    {
      name: check_img.filename,
      path: check_img_path
    }
  end

  private

  def check_img_path
    Rails.application.routes.url_helpers.rails_blob_path(
      check_img, only_path: true
    )
  end
end
