class DogSerializer < ActiveModel::Serializer
  delegate :doc_img, to: :object

  attributes :id, :breed_id, :name, :color_str, :color_id,
    :date_of_birth, :document, :breeder, :owner, :sex,
    :mother_name, :father_name, :user_id, :hair_type_id,
    :titles, :chip, :stigma, :document_image

  def document_image
    return {} unless doc_img.attached?

    {
      name: doc_img.filename,
      path: doc_img_path
    }
  end

  private

  def doc_img_path
    Rails.application.routes.url_helpers.rails_blob_path(
      doc_img, only_path: true
    )
  end
end
