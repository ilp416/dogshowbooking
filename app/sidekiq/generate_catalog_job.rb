class GenerateCatalogJob
  include Sidekiq::Job

  def perform(event_id, prices)
    Catalogs::Docx.call Event.find(event_id), prices: prices
  end
end
