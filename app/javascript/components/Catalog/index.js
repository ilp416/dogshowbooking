import React, { useState, useEffect } from 'react';
import I18n from "i18n-js";
import ReactSafeHtml from "react-safe-html"

import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListSubheader from '@material-ui/core/ListSubheader';
import TextField from '@material-ui/core/TextField';

import { BREEDS } from 'support/Breeds';
import { BREED_LIST } from 'support/Breeds';
import API from 'support/API';
import Dog from '../RegistrationList/Dog';
import RegistrationItem from '../RegistrationList/RegistrationItem';

import MuiDialogTitle from '@material-ui/core/DialogTitle';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import Typography from '@material-ui/core/Typography';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import Form from '../dog/Form';

export default function PriceForm(props) {
  const [event, setEvent] = useState({})
  const [editDog, setEditDog] = useState({})

  const handleEditDog = (dog) => {
    setEditDog(dog)
  }
const handleEditDogClose = () => { setEditDog({})
  }

  const handleDogUpdated = (dog) => {
    handleEditDogClose()
    fetchEvent()
  }

  const renderEditDog = () => {
    if (editDog.id === undefined) { return null; }
    return (
      <Dialog open onClose={(e) => handleEditDogClose()}>
        <MuiDialogTitle>
          <div className="d-flex justify-content-between align-items-center">
            <Typography variant="h6">{I18n.t('dogs.edit.title')}</Typography>
            <IconButton aria-label="close" onClick={(e) => handleEditDogClose()}>
              <CloseIcon />
            </IconButton>
          </div>
        </MuiDialogTitle>
        <DialogContent>
          <Form
            admin
            dog={editDog}
            submitButtonTitle={I18n.t('save')}
            onSave={(dog) => handleDogUpdated(dog) }
          />
        </DialogContent>
      </Dialog>
    )
  }

  const fetchEvent = () => {
    API({
      url: `/admin/events/${props.event.id}/catalog.json`,
      method: "GET",
      onSuccess: setEvent
    })
  }

  let prev = {}
  let dog = {}
  let currentNum = 0

  useEffect(() => {
    fetchEvent()
  }, [true]);

  const groupLabel = (reg) => {
    if (prev.groupId == reg.groupId) { return null }
    if (event.rank != 'universal') { return null }

    prev = { groupId: reg.groupId }
    return(
      <h2 className="breed_group">
        {I18n.t(`groups.${reg.groupId}`)}
      </h2>
    )
  }

  const qrCode = (url) => {
    return(
      <img className="mx-auto qr-code"
           src={`https://chart.googleapis.com/chart?cht=qr&chs=400x400&choe=UTF-8&chld=H&chl=${url}`}
      />
    )
  }

  const breedLabel = (reg) => {
    if (prev.breedId == reg.dog.breedId) { return null }
    console.log(reg.dog.breedId)
    prev = { ...prev,
      breedId: reg.dog.breedId,
      humanHairTypeGroup: undefined,
      humanColorGroup: undefined,
      gender: undefined,
      showClass: undefined
    };
    return(
      <div>
        <h3 className="breed">
          {I18n.t(`breeds.${reg.dog.breedId}`)}
        </h3>
        {qrCode(`https://mydog.show/catalogs/${props.event.id}#breed${reg.dog.breedId}`)}
      </div>
    )
  }

  const hairTypeLabel = (reg) => {
    console.log('humanHairTypeGroup: ' + reg.dog.humanHairTypeGroup)
    if (!reg.dog.humanHairTypeGroup || (prev.humanHairTypeGroup == reg.dog.humanHairTypeGroup)) { return null }
    prev = { ...prev,
      humanHairTypeGroup: reg.dog.humanHairTypeGroup,
      humanColorGroup: undefined,
      gender: undefined,
      showClass: undefined
    };
    return(
      <h4 className="hair_type">
        {reg.dog.humanHairTypeGroup}
      </h4>
    )
  }

  const colorGroupLabel = (reg) => {
    console.log('humanColorGroup: ' + reg.dog.humanColorGroup)
    if (!reg.dog.humanColorGroup || (prev.humanColorGroup == reg.dog.humanColorGroup)) { return null }
    prev = { ...prev,
      humanColorGroup: reg.dog.humanColorGroup,
      gender: undefined,
      showClass: undefined
    };
    return(
      <h4 className="color">
        {reg.dog.humanColorGroup}
      </h4>
    )
  }

  const genderLabel = (reg) => {
    if (prev.gender == reg.dog.gender) { return null }
    prev = { ...prev,
      gender: reg.dog.gender,
      showClass: undefined
    };
    return(
      <h5 className="gender">
        {I18n.t(`plural_sex.${reg.dog.sex}`)}
      </h5>
    );
  }

  const showClassLabel = (reg) => {
    if (prev.showClass == reg.showClass) { return null }
    prev = { ...prev,
      showClass: reg.showClass
    };
    return(
      <h5 className="show_class">
      {I18n.t(`enumerize.registration.show_class.${reg.showClass}`)}
      </h5>
    )
  }

  const catalogNumber = (reg) => {
    if (reg.catalogNumber) { return reg.catalogNumber }
    return(currentNum += 1)
  }

  const catalogNumberClass = (reg) => {
    return(reg.catalogNumber ? 'real' : 'virtual')
  }

  return (
    <div className="catalog">
      <h1>
        <ReactSafeHtml html={ props.event.name }/>
      </h1>
      <h3>
        <ReactSafeHtml html={ props.club.fullname }/>
      </h3>
      {qrCode(`https://mydog.show/catalogs/${props.event.id}`)}

      { event.registrations && event.registrations.map(reg => {
        return [
          groupLabel(reg),
          breedLabel(reg),
          hairTypeLabel(reg),
          colorGroupLabel(reg),
          genderLabel(reg),
          showClassLabel(reg),
          <div className="row dog_and_reg">
            <div key={`dog-${reg.dog.id}`} className="dog col-md-6 d-flex justify-content-start">
              <div className={`catalog-number ${catalogNumberClass(reg)}`}>
                {catalogNumber(reg)}
              </div>
              <Dog
                className="container-fluid p-0"
                dog={reg.dog}
                onEditDog={reg.catalogNumber ? undefined : handleEditDog}
              />
            </div>
            <div className="reg col-md-6">
              <RegistrationItem key={reg.id} registration={reg} dog={dog}
                onUpdate={(data) => fetchEvent()}
              />
            </div>
          </div>
        ]
      })}
      { renderEditDog() }
    </div>
  )
}
