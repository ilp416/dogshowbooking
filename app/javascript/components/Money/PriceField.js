import React, { useRef } from "react";
import I18n from "i18n-js";

import Switch from '@material-ui/core/Switch';
import { AVAILABLE_CURRENCIES } from 'support/Breeds';

export default function PriceField(props) {
  const value = props.price.value || ''
  const currency = props.price.currency || AVAILABLE_CURRENCIES[0]

  const handleValueChange = (e) => {
    props.onChange({ value: parseInt(e.target.value), currency: currency })
  }

  const handleCurrencySelect = (e, newCurrencyValue) => {
    e.preventDefault();
    props.onChange({ value: value, currency: newCurrencyValue })
  }

  return (
    <div className="input-group">
      <input type="text"
        name={props.fieldName + '[value]'}
        className="price form-control form-control-sm border-primary text-end"
        value={value}
        onChange={handleValueChange}
      />
      <input type="hidden"
        name={props.fieldName + '[currency]'}
        value={currency}
      />
      <button className="btn btn-outline-primary btn-sm dropdown-toggle"
        data-bs-toggle="dropdown"
        aria-expanded="false"
      >
        {currency}
      </button>
      <ul className="dropdown-menu dropdown-menu-end">
        {AVAILABLE_CURRENCIES.map((curr) => (
          <li key={curr}>
            <a className="dropdown-item" href="#" onClick={(e) => handleCurrencySelect(e, curr)}>
              {curr}
            </a>
          </li>
        ))}
      </ul>
    </div>
  )
}
