import React from 'react';
import I18n from "i18n-js";
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';
import FormHelperText from '@material-ui/core/FormHelperText';
import { BREED_LIST } from 'support/Breeds';

const options = BREED_LIST.map((breed) => {
  const title = I18n.t('breeds.' + breed.key);
  const groupId = parseInt(I18n.t('group_ids.' + breed.group));
  const groupTitle = I18n.t('groups.' + breed.group);
  return {
    key: breed.key, title: title, groupId: groupId, groupTitle: groupTitle
  };
});

const sortedOptions = options.sort(function(a, b){
  if (a.groupId < b.groupId) { return -1; }
  if (a.groupId > b.groupId) { return 1; }
  if (a.groupId == b.groupId) {
    return(a.title < b.title ? -1 : 1)
  }
});

class BreedComboBox extends React.Component {
  currentValue() {
    return options.find((element) => {
      return element.key == this.props.value
    });
  }

  handleChange(newBreedObj) {
    // console.log('Breed changed');
    // console.log(newBreedObj);
    if (newBreedObj) {
      this.props.onChange(newBreedObj.key);
    }
  }

  render () {
    return (
      <div>
        <Autocomplete
          id="breed-select"
          required
          options={sortedOptions}
          groupBy={(option) => option.groupTitle}
          getOptionLabel={(option) => option.title}
          onChange={(event, newBreedObj) => this.handleChange(newBreedObj)}
          value={this.currentValue() || null}
          renderInput={(params) => <TextField {...params} label={this.props.label} variant={this.props.variant}/>}
        />
        <FormHelperText error>
          { this.props.helperText }
        </FormHelperText>
      </div>
    )
  }
}

BreedComboBox.defaultProps = {
  label: I18n.t('activerecord.attributes.dog.breed'),
  variant: 'filled'
}

export default BreedComboBox
