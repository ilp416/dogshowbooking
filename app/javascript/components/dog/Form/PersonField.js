import React from 'react';
import I18n from "i18n-js";
import PropTypes from "prop-types"
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Link from '@material-ui/core/Link';

class PersonField extends React.Component {
  currentUserToVal() {
    return 'Хрен с горы';
  }

  clickIam (e) {
    e.preventDefault();
    this.props.onChange(this.props.helperValue);
  }

  renderHelperLink() {
    if (this.props.helperValue) {
      return (
        <div>
          <Link href="#" className="reactive" size="small" onClick={(e) => this.clickIam(e)}>
            <small>
              {this.props.label + ' ' +  I18n.t('is_i_am')}
            </small>
          </Link>
        </div>
      )
    }
  }

  render () {
    const {helperValue, ...textFieldProps} = this.props;
    return (
      <div>
        <TextField
          {...textFieldProps}
          onChange={(event) => this.props.onChange(event.target.value)}
        />
        {this.renderHelperLink()}
      </div>
    )
  }
}

PersonField.defaultProps = {
  value: '',
  className: 'mb-0',
  fullWidth: true
}
PersonField.propTypes = {
  value: PropTypes.string,
}
export default PersonField
