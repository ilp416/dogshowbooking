import React from 'react';
import I18n from "i18n-js";

import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';

import API from 'support/API'

export default function NameField(props) {
  const [open, setOpen] = React.useState(false);
  const [options, setOptions] = React.useState([]);

  const handleChange = (val) => {
    setOpen(false)
    props.onDogFound(val)
  }

  const updateOptions = (dogs) => {
    setOptions(dogs)
    if (dogs.length) { setOpen(true) }
    else { setOpen(false) }
  }

  const handleInputChange = (event, val, reason) => {
    if (reason != 'input') {
      setOpen(false)
      return null
    }
    if (!props.dog.id && val.length > 4) {
      API({
        url: `/dogs/search.json?search=${val}`,
        method: "GET",
        onSuccess: updateOptions
      })
    } else {
      updateOptions([])
    }
    props.onChange(val)
  }

  return (
    <Autocomplete
      value={props.value}
      freeSolo={true}
      open={open}
      onOpen={() => {
        setOpen(true);
      }}
      onClose={() => {
        setOpen(false);
      }}
      blurOnSelect
      getOptionLabel={(option) => typeof(option) === 'string' ? option : option.name}
      options={options}
      onChange={(event, dogObj) => handleChange(dogObj)}
      onInputChange={(e, val, r) => handleInputChange(event, val, r)}
      renderInput={(params) => (
        <TextField
          {...params}
          label={props.label}
          error={props.error}
          helperText={props.helperText}
          variant={props.variant}
        />
      )}
    />
  );
}
