import 'date-fns';
import ruLocale from "date-fns/locale/ru";
// import enLocale from "date-fns/locale/en-US";
import React from 'react';
import I18n from "i18n-js";
import DateFnsUtils from '@date-io/date-fns';
import FormHelperText from '@material-ui/core/FormHelperText';


import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
  DatePicker
} from "@material-ui/pickers";

const localeMap = {
  // en: enLocale,
  ru: ruLocale,
};

class DateOfBirthField extends React.Component {
  render () {
    return (
      <MuiPickersUtilsProvider utils={ DateFnsUtils } locale={ localeMap['ru'] }>
        <KeyboardDatePicker
          { ...this.props }
          cancelLabel={ I18n.t('cancel') }
          okLabel={ I18n.t('ok') }
          disableFuture
          autoOk
          onChange={ (value) => this.props.onChange(value.toDateString()) }
        />
      </MuiPickersUtilsProvider>
    )
  }
}

DateOfBirthField.defaultProps = {
  label: I18n.t('activerecord.attributes.dog.date_of_birth'),
  format: "dd.MM.yyyy",
  value: null,
}

export default DateOfBirthField
