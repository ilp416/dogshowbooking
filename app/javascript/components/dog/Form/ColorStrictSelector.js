import React from 'react';
import I18n from "i18n-js";
import PropTypes from "prop-types"
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';
import FormHelperText from '@material-ui/core/FormHelperText';
import { BREED_COLORS } from 'support/Breeds';

class ColorStrictSelector extends React.Component {
  availableColors () {
    return(BREED_COLORS[this.props.breed]);
  }

  options () {
    return(
      this.availableColors().map((colorKey) => {
        const title = I18n.t('colors.' + colorKey);

        return { key: colorKey, title: title }
      })
    );
  }

  sortedOptions () {
    return this.options().sort(function(a, b){
      return(a.title < b.title ? -1 : 1)
    });
  }

  currentValue() {
    return this.options().find((element) => {
      return element.key == this.props.colorId
    });
  }

  handleChange(newColorObj) {
    // console.log('Breed changed');
    // console.log('ColorStrictSelector:' + JSON.stringify(newColorObj, null, 2));
    if (newColorObj) {
      this.props.onChange(newColorObj.key);
    }
  }

  renderOption (params) {
    return(
      <TextField {...params} label={this.props.label} variant={this.props.variant}/>
    )
  }

  render () {
    return (
      <div>
        <Autocomplete
          required={ this.props.required }
          id="color-select"
          options={this.sortedOptions()}
          getOptionLabel={(option) => option.title}
          onChange={(event, newColorObj) => this.handleChange(newColorObj)}
          value={this.currentValue() || null}
          error={this.props.error}
          renderInput={(params) => this.renderOption(params)}
        />
        <FormHelperText error>
          { this.props.helperText }
        </FormHelperText>
      </div>
    )
  }
}

ColorStrictSelector.defaultProps = {
  label: I18n.t('activerecord.attributes.dog.color'),
  variant: 'filled',
  required: undefined,
}

ColorStrictSelector.propTypes = {
  breed: PropTypes.string,
  colorId: PropTypes.string
}

export default ColorStrictSelector
