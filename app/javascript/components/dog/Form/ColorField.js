import React from 'react';
import I18n from "i18n-js";
import PropTypes from "prop-types"
import { makeStyles } from '@material-ui/core/styles';
import ColorStrictSelector from './ColorStrictSelector'
import ColorFreeInput from './ColorFreeInput'
import { BREED_COLORS } from 'support/Breeds';

class ColorField extends React.Component {
  availableColors () {
    return(BREED_COLORS[this.props.breed]);
  }

  isStrictColors () {
    return this.availableColors() && this.availableColors().length;
  }

  handleChange(newColor) {
    // console.log('ColorField: ' + newColor);
    const attrName = this.isStrictColors() ? 'colorId' : 'colorStr';
    this.props.onChange(attrName, newColor);
  }

  render () {
    if (this.isStrictColors()) {
      return (
        <ColorStrictSelector
          {...this.props}
          onChange={(newColor) => this.handleChange(newColor)}
        />
      )
    }
    return (
      <ColorFreeInput
        {...this.props}
        fullWidth
        onChange={(newColor) => this.handleChange(newColor)}
      />
    )
  }
}

ColorField.defaultProps = {
  label: I18n.t('activerecord.attributes.dog.color'),
  variant: 'filled',
  value: ''
}

ColorField.propTypes = {
  breed: PropTypes.string,
  colorId: PropTypes.string,
  colorStr: PropTypes.string
}

export default ColorField
