import React from 'react';
import Radio from '@material-ui/core/Radio';
import I18n from "i18n-js";
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import FormHelperText from '@material-ui/core/FormHelperText';

class SexField extends React.Component {
  humanAttr (attr) {
    return I18n.t('enumerize.dog.sex.' + attr);
  }

  render () {
    const {error, helperText, ...radioFieldProps} = this.props;
    return (
      <FormControl component="fieldset">
        <RadioGroup row
          { ...radioFieldProps }
          aria-label="gender"
          onChange={ (event) => this.props.onChange(event.target.value) }
        >
          <FormControlLabel value="male" control={<Radio color='primary' value="male" />} label={this.humanAttr('male')} />
          <FormControlLabel value="female" control={<Radio color='primary' value="female" />} label={this.humanAttr('female')} />
        </RadioGroup>
        <FormHelperText error>
          { this.props.helperText }
        </FormHelperText>
      </FormControl>
    )
  }
}

SexField.defaultProps = {
  label: I18n.t('activerecord.attributes.dog.sex'),
  color: 'primary'
}
export default SexField
