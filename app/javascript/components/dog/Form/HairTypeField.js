import React from 'react';
import I18n from "i18n-js";
import PropTypes from "prop-types"
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';
import FormHelperText from '@material-ui/core/FormHelperText';
import { BREED_HAIR_TYPES } from 'support/Breeds';

class HairTypeField extends React.Component {
  availableHairTypes () {
    return(BREED_HAIR_TYPES[this.props.breed]);
  }

  options () {
    return(
      this.availableHairTypes().map((key) => {
        const title = I18n.t('hair_types.' + key);

        return { key: key, title: title }
      })
    );
  }

  sortedOptions () {
    return this.options().sort(function(a, b){
      return(a.title < b.title ? -1 : 1)
    });
  }

  currentValue() {
    return this.options().find((element) => {
      return element.key == this.props.hairTypeId
    });
  }

  handleChange(hairTypeObj) {
    if (hairTypeObj) {
      this.props.onChange(hairTypeObj.key);
    }
  }

  renderOption (params) {
    return(
      <TextField {...params} label={this.props.label} variant={this.props.variant}/>
    )
  }

  render () {
    if (this.availableHairTypes() && this.availableHairTypes().length) {
      return (
        <div>
          <Autocomplete
            required={ this.props.required }
            id="hair-type-select"
            options={this.sortedOptions()}
            getOptionLabel={(option) => option.title}
            onChange={(event, hairTypeObj) => this.handleChange(hairTypeObj)}
            value={this.currentValue()}
            error={this.props.error}
            renderInput={(params) => this.renderOption(params)}
          />
          <FormHelperText error>
            { this.props.helperText }
          </FormHelperText>
        </div>
      )
    }
    return null;
  }
}

HairTypeField.defaultProps = {
  label: I18n.t('activerecord.attributes.dog.hair_type'),
  variant: 'filled',
}

HairTypeField.propTypes = {
  breed: PropTypes.string,
  hairTypeId: PropTypes.string
}

export default HairTypeField
