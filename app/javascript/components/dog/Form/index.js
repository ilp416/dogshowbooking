import React from 'react';
import I18n from "i18n-js";
import { makeStyles } from '@material-ui/core/styles';
import PropTypes from "prop-types"

import FormControl from '@material-ui/core/FormControl';
import TextField from '@material-ui/core/TextField';
import Divider from '@material-ui/core/Divider';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';

import NameField from './NameField'
import BreedComboBox from './BreedComboBox'
import ColorField from './ColorField'
import HairTypeField from './HairTypeField'
import SexField from './SexField'
import DocumentField from './DocumentField'
import DocImgField from './DocImgField'
import DateOfBirthField from './DateOfBirthField'
import PersonField from './PersonField'

const token = document.querySelector('meta[name="csrf-token"]').content;

class Form extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      dog: props.dog,
      errors: {}
    }
  }

  dogPath() {
    const adminPart = this.props.admin ? '/admin' : ''
    const path = `${adminPart}/dogs`

    if (parseInt(this.state.dog.id)) { return `${path}/${this.state.dog.id}` }

    return path
  }

  handleSubmit(e) {
    e.preventDefault();
    // console.log(JSON.stringify(this.state))

    fetch(this.dogPath(), {
      method: parseInt(this.state.dog.id) ? "PUT" : "POST",
      body: JSON.stringify(this.state),
      headers: {
        'Key-Inflection': 'camel',
        'X-CSRF-Token': token,
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
    })
    .then(response => {
      const ok = response.ok;
      const data = response.json();
      return Promise.all([ok, data]);
    })
    .then(res => {
      return { ok: res[0], data: res[1] }
    })
    .then(result => {
      console.log(result.data)
      if (result.ok) {
        const dog = result.data
        if(typeof this.props.onSave === 'string'){
          window[this.props.onSave](dog)
        }else{
          this.props.onSave(dog)
        }
      }
      else {
        this.setState(prevState => ({
          ...prevState,
          errors: result.data.errors
        }))
      }
    })
    .catch(error => {
        console.log(error);
    });
  }

  attrLabel (attrName) {
    return(I18n.t('activerecord.attributes.dog.' + attrName));
  }

  handleChangeAttr (attr, value) {
    if (attr == 'colorStr') {
      this.setState(prevState => ({
        errors: { ...prevState.errors, colorId: undefined },
        dog: { ...prevState.dog, colorId: null }
      }))
    }
    if (attr == 'colorId') {
      this.setState(prevState => ({
        errors: { ...prevState.errors, colorStr: undefined },
        dog: { ...prevState.dog, colorStr: null }
      }))
    }
    this.setState(prevState => ({
      ...prevState,
      errors: { ...prevState.errors, [attr]: undefined },
      dog: { ...prevState.dog, [attr]: value }
    }))
    console.log(this.state.dog);
  }

  isError(field) {
    const err = this.state.errors[field];
    return(err && !!err.length ? true : undefined);
  }

  humanError(field){
    const err = this.state.errors[field];
    const emptyValue = ['owner', 'breeder'].includes(field) ? undefined : ' '
    return((err && err.join(', ')) || emptyValue);
  }

  handleDogFound(dog) {
    if (!dog) { return null }
    if (this.state.dog.id) { return null }

    this.setState(prevState => ({
      ...prevState,
      dog: {
        ...dog,
        id: dog.userId == this.props.currentUser.id ? dog.id : undefined
      }
    }))
  }

  render () {
    return (
      <form >
        <NameField
          value={this.state.dog.name || ''}
          dog={this.state.dog}
          onChange={(val) => this.handleChangeAttr('name', val)}
          onDogFound={(dog) => this.handleDogFound(dog)}
          label={this.attrLabel('name_with_note')}
          error={this.isError('name')}
          helperText={this.humanError('name')}
          variant={this.props.variant}
        />
        <BreedComboBox required
          value={this.state.dog.breedId || null}
          variant={this.props.variant}
          error={this.isError('breedId')}
          helperText={this.humanError('breedId')}
          onChange={(val) => this.handleChangeAttr('breedId', val)}
        />
        <div className="row">
          <div className="col-sm-12 col-md-6">
            <ColorField required
              colorId={this.state.dog.colorId}
              colorStr={this.state.dog.colorStr}
              breed={this.state.dog.breedId}
              variant={this.props.variant}
              error={this.isError('colorId') || this.isError('colorId')}
              helperText={this.humanError('colorId') || this.humanError('colorStr')}
              onChange={(attr, val) => this.handleChangeAttr(attr, val)}
            />
          </div>
          <div className="col-sm-12 col-md-6">
            <HairTypeField required
              hairTypeId={this.state.dog.hairTypeId}
              breed={this.state.dog.breedId}
              variant={this.props.variant}
              error={this.isError('hairTypeId')}
              helperText={this.humanError('hairTypeId')}
              onChange={ (val) => this.handleChangeAttr('hairTypeId', val) }
            />
          </div>
        </div>
        <SexField required
          value={this.state.dog.sex || null}
          error={this.isError('sex')}
          helperText={this.humanError('sex')}
          onChange={(val) => this.handleChangeAttr('sex', val)}
        />
        <DocumentField required
          value={this.state.dog.document || ''}
          error={this.isError('document')}
          helperText={this.humanError('document')}
          onChange={(val) => this.handleChangeAttr('document', val)}
        />
        <div>
          <DocImgField required
            label={this.attrLabel('doc_img')}
            value={this.state.dog.documentImage || ''}
            error={this.isError('docImg')}
            helperText={this.humanError('docImg')}
            onChange={(val) => this.handleChangeAttr('docImg', val)}
          />
        </div>
        <Divider variant="middle" className="my-2" />
        <DateOfBirthField required
          value={this.state.dog.dateOfBirth}
          error={this.isError('dateOfBirth')}
          helperText={this.humanError('dateOfBirth')}
          onChange={(val) => this.handleChangeAttr('dateOfBirth', val)}
        />
        <Divider variant="middle" className="my-2" />
        <TextField required fullWidth
          label={this.attrLabel('father_name')}
          variant={this.props.variant}
          value={this.state.dog.fatherName || ''}
          error={this.isError('fatherName')}
          helperText={this.humanError('fatherName')}
          onChange={(val) => this.handleChangeAttr('fatherName', val.target.value)}
        />
        <TextField required fullWidth
          label={this.attrLabel('mother_name')}
          variant={this.props.variant}
          value={this.state.dog.motherName || ''}
          error={this.isError('motherName')}
          helperText={this.humanError('motherName')}
          onChange={(val) => this.handleChangeAttr('motherName', val.target.value)}
        />
        <Divider variant="middle" className="my-2" />
        <div className="row">
          <div className="col-sm-12 col-md-6">
            <PersonField required
              value={this.state.dog.breeder}
              helperValue={this.props.currentUser.initials}
              label={this.attrLabel('breeder')}
              variant={this.props.variant}
              error={this.isError('breeder')}
              helperText={this.humanError('breeder')}
              onChange={(val) => this.handleChangeAttr('breeder', val)}
            />
          </div>
          <div className="col-sm-12 col-md-6">
            <PersonField required
              value={this.state.dog.owner}
              helperValue={this.props.currentUser.initials}
              label={this.attrLabel('owner')}
              variant={this.props.variant}
              error={this.isError('owner')}
              helperText={this.humanError('owner')}
              onChange={(val) => this.handleChangeAttr('owner', val)}
            />
          </div>
        </div>
        <Divider variant="middle" className="my-2" />
        <TextField required fullWidth
          label={this.attrLabel('titles')}
          variant={this.props.variant}
          value={this.state.dog.titles || ''}
          error={this.isError('titles')}
          helperText={this.humanError('titles')}
          onChange={(val) => this.handleChangeAttr('titles', val.target.value)}
        />
        <div className="row">
          <div className="col-sm-12 col-md-6">
            <TextField required fullWidth
              label={this.attrLabel('stigma')}
              variant={this.props.variant}
              value={this.state.dog.stigma || ''}
              error={this.isError('stigma')}
              helperText={this.humanError('stigma')}
              onChange={(val) => this.handleChangeAttr('stigma', val.target.value)}
            />
          </div>
          <div className="col-sm-12 col-md-6">
            <TextField required fullWidth
              label={this.attrLabel('chip')}
              variant={this.props.variant}
              value={this.state.dog.chip || ''}
              error={this.isError('chip')}
              helperText={this.humanError('chip')}
              onChange={(val) => this.handleChangeAttr('chip', val.target.value)}
            />
          </div>
        </div>
        <div className="text-center">
          <Button variant="contained" color="primary" disabled={this.props.disabled} onClick={(e) => this.handleSubmit(e)}>
            { this.props.submitButtonTitle }
          </Button>
          <div color="primary" className="fst-italic">
            { this.props.disabled || this.props.submitButtonNote }
          </div>
        </div>
      </form>
    )
  }
}

Form.defaultProps = {
  variant: 'standard',
  dog: {
    breed: undefined,
    breedId: undefined,
    name: undefined,
    colorStr: undefined,
    colorId: undefined,
    hairTypeId: undefined,
    sex: undefined,
    document: undefined,
    breeder: undefined,
    owner: undefined,
    motherName: undefined,
    fatherName: undefined,
  },
  currentUser: {}
}
Form.propTypes = {
  breed: PropTypes.string,
  breedId: PropTypes.string,
  name: PropTypes.string,
  colorStr: PropTypes.string,
  colorId: PropTypes.string,
  sex: PropTypes.string,
  document: PropTypes.string,
  dateOfBirth: PropTypes.instanceOf(Date),
  breeder: PropTypes.string,
  owner: PropTypes.string,
  motherName: PropTypes.string,
  fatherName: PropTypes.string
};
export default Form
