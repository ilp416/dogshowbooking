import React from 'react';
import I18n from "i18n-js";
import PropTypes from "prop-types"
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';

class ColorFreeInput extends React.Component {
  handleChange(event) {
    const val = event.target.value
    // console.log(val);
    this.props.onChange(val);
  }

  adaptiveValue () {
    if (this.props.colorId) {
      return I18n.t('colors.' + this.props.colorId)
    }

    return this.props.colorStr
  }

  render () {
    const {colorStr, colorId, ...textFieldProps} = this.props;
    return (
      <TextField
        { ...textFieldProps }
        value={this.adaptiveValue() || ''}
        onChange={(event) => this.handleChange(event)}
      />
    )
  }
}

ColorFreeInput.defaultProps = {
  label: I18n.t('activerecord.attributes.dog.color'),
  variant: 'filled',
  value: '',
}

ColorFreeInput.propTypes = {
  breed: PropTypes.string,
  colorStr: PropTypes.string,
  colorId: PropTypes.string
}

export default ColorFreeInput
