import React from 'react';
import I18n from "i18n-js";

import IconButton from '@material-ui/core/IconButton';
import FormHelperText from '@material-ui/core/FormHelperText';
import { DirectUploadProvider } from 'react-activestorage-provider'

export default function DocImgField(props) {
  const [value, setValue] = React.useState(props.value);
  const handleAttachment = (imageBlob) => {
    props.onChange(imageBlob[0])
  }

  const printableValue = () => {
    if(!value) { return }
    return (
      value.name + ' (' + value.size + ')'
    )
  }

  const handleFileSelected = (e, handleUpload) => {
    setValue({path: URL.createObjectURL(e.target.files[0])})
    handleUpload(e.currentTarget.files)
  }

  const imgInput = (handleUpload, ready) => {
    return(
      <input
        hidden
        id='doc-img-input'
        type="file"
        onChange={e => handleFileSelected(e, handleUpload)}
      />
    )
  }

  const emptyImg = (handleUpload, ready) => {
    return(
      <div>
        { imgInput(handleUpload, ready) }
        <label className={"doc-preview doc-preview__empty"} htmlFor="doc-img-input">
          <IconButton component="span">
            <i className={"fa fa-file-upload"}/>
          </IconButton>
        </label>
      </div>
    )
  }

  const preview = (handleUpload, ready) => {
    const divStyle = { backgroundImage: 'url(' + value.path + ')'}
    const previewType = value.path ? 'real' : 'empty'
    const iconClass = value.path ? 'fa-pen' : 'fa-file-upload'
    return(
      <a href={value.path} target="_blank" className={"doc-preview doc-preview__" + previewType} style={divStyle}>
        { imgInput(handleUpload, ready) }
        <label htmlFor="doc-img-input">
          <IconButton component="span">
            <i className={"fa " + iconClass}/>
          </IconButton>
        </label>
      </a>
    )
  }

  return (
    <div>
      <label className="MuiFormLabel-root MuiInputLabel-root MuiInputLabel-animated MuiInputLabel-shrink MuiFormLabel-filled">
        {props.label}
        {props.required && '*'}
      </label>
      <DirectUploadProvider
        onSuccess={handleAttachment}
        render={({ handleUpload, uploads, ready }) => (
          <div>
            {value.path ? preview(handleUpload, ready) : emptyImg(handleUpload, ready)}
            {uploads.map(upload => {
              switch (upload.state) {
                case 'waiting':
                  return <p key={upload.id}>Waiting to upload {upload.file.name}</p>
                case 'uploading':
                  return (
                    <p key={upload.id}>
                      Uploading {upload.file.name}: {upload.progress}%
                    </p>
                  )
                case 'error':
                  return (
                    <p key={upload.id}>
                      Error uploading {upload.file.name}: {upload.error}
                    </p>
                  )
                case 'finished':
                  return (
                    <p key={upload.id}>Finished uploading {upload.file.name}</p>
                  )
              }
            })}
          </div>
        )}
      />
      <FormHelperText error>
        { props.helperText }
      </FormHelperText>
    </div>
  )
}
