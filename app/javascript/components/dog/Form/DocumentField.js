import React from 'react';
import I18n from "i18n-js";
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import TextField from '@material-ui/core/TextField';
import Checkbox from '@material-ui/core/Checkbox';
import FormGroup from '@material-ui/core/FormGroup';

class DocumentField extends React.Component {
  humanDocAlt (val) {
    return I18n.t('dogs.document_alts.' + val);
  }

  isCheckBoxValue (value) {
    return this.props.checkBoxValues.includes(value);
  }

  isCheckBoxText (value) {
    const checkBoxTexts = this.props.checkBoxValues.map((val) => (
      this.humanDocAlt(val)
    ))

    return checkBoxTexts.includes(value);
  }

  textValue (value) {
    if (this.isCheckBoxValue(value)) {
      return this.humanDocAlt(value);
    }

    return value || '';
  }

  handleCheckBoxChanged (event, value) {
    const checked = event.target.checked
    if (value) {
      this.props.onChange(value);
    }
  }

  renderCheckBox (value) {
    return (
      <FormControlLabel
        key={'check_' + value}
        control={
          <Checkbox
            checked={this.props.value == value}
            onChange={(event) => this.handleCheckBoxChanged(event, value)}
            size={this.props.size}
            color="primary"
          />
        }
        label={this.humanDocAlt(value)}
      />
    )
  }

  handleTextFieldChanged (event) {
    const val = event.target.value

    if (!this.isCheckBoxValue(val) && !this.isCheckBoxText(val)) {
      this.props.onChange(val);
    }
  }

  render () {
    const { checkBoxValues, ...textFieldProps } = this.props;
    return (
      <FormGroup row component="fieldset">
        <TextField
          { ...textFieldProps }
          fullWidth={false}
          value={this.textValue(this.props.value)}
          onChange={(event) => this.handleTextFieldChanged(event)}
        />
        {
          this.props.checkBoxValues.map((value) => (
            this.renderCheckBox(value)
          ))
        }
      </FormGroup>
    )
  }
}

DocumentField.defaultProps = {
  label: I18n.t('activerecord.attributes.dog.document'),
  value: '',
  size: 'small',
  checkBoxValues: ['exchange', 'puppy_card'],
}
export default DocumentField
