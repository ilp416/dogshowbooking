import React from "react"
import { useEffect } from 'react';
import API from 'support/API'
import ReactSafeHtml from "react-safe-html"
import I18n from "i18n-js";

import RegistrationForm from './RegistrationForm'
import ClubTitle from '../Event/ClubTitle'
import DateCalendarPage from '../DateCalendarPage'

export default function Registrations(props) {
  const dog = props.dog
  const [registrations, setRegistrations] = React.useState([]);
  const [clubs, setClubs] = React.useState([]);
  const [events, setEvents] = React.useState([]);

  const updateRegistrations = (data) => {
    setClubs(data.clubs)
    setEvents(data.events)
    setRegistrations(data.dateRegistrations)
  }

  const fetchRegistrations = () => {
    API({
      url: `/dogs/${dog.id}/registrations`,
      method: "GET",
      onSuccess: updateRegistrations
    })
  }

  useEffect(() => {
    fetchRegistrations()
  }, [])

  const clubAndDateTitle = (date, clubId) => {
    const dateTitle = I18n.l('date.formats.pretty', date)
    const clubTitle = clubs[clubId + ''].fullname
    return(
      <h5 className="pt-5">
        <ReactSafeHtml html={ dateTitle + ' ' + clubTitle }/>
      </h5>
    )
  }

  return (
    <React.Fragment>
      { registrations && registrations.map((dateRegs) => (
        <div key={`regs_${dateRegs.date}`}>
          { dateRegs.clubsRegistrations.map((clubRegs) => (
            <div className="card mb-5" key={`regs_${dateRegs.date}_${clubRegs.clubId}`}>
              <div className="card-title title d-flex">
                <DateCalendarPage date={dateRegs.date} />
                <ClubTitle
                  club={clubs[clubRegs.clubId + '']}
                  info={clubRegs.info}
                />
              </div>
              <div className="registrations border-top">
                { clubRegs.registrations.map((reg) =>(
                  <RegistrationForm
                    disabled={!!parseInt(reg.id)}
                    onChange={(e) => fetchRegistrations()}
                    key={'registrationForm' + reg.eventId}
                    dog={dog}
                    registration={reg}
                    event={events[reg.eventId + '']}
                  />
                ))}
              </div>
            </div>
          ))}
        </div>
      ))}
    </React.Fragment>
  )
}
