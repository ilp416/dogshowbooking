import React from "react"
import PropTypes from "prop-types"
import I18n from "i18n-js";

import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormHelperText from '@material-ui/core/FormHelperText';

class SelectShowClass extends React.Component {

  showClassLabel(showClass) {
    return(
      <div className="pb-2">
        { I18n.t('enumerize.registration.show_class.' + showClass) }
        <FormHelperText>
          { I18n.t('registration.show_class_note.' + showClass) }
        </FormHelperText>
      </div>
    )
  }

  render () {
    return (
      <RadioGroup
        aria-label="show class" name="showClass"
        disabled={ this.props.disabled }
        value={ this.props.value }
        onChange={ this.props.onChange }
      >

        <FormHelperText error={ this.props.error }>
          { this.props.errorMsg }
        </FormHelperText>

        { this.props.showClasses.map(showClass => (
          <FormControlLabel
            disabled={ this.props.disabled }
            key={ showClass }
            className='align-items-start pt-0 show-class-label'
            value={ showClass }
            control={ <Radio color='primary' /> }
            label={ this.showClassLabel(showClass) }
          />
        ))}
      </RadioGroup>
    )
  }
}
SelectShowClass.defaultProps = {
  color: 'primary'
}
export default SelectShowClass
