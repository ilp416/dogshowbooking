import React from "react"
import PropTypes from "prop-types"
import I18n from "i18n-js";

import TextField from '@material-ui/core/TextField';

class NoteField extends React.Component {

  render() {
    if (this.props.disabled && !this.value) {
      return(null);
    }
    return(
      <TextField
        { ...this.props }
        fullWidth
        multiline
        maxRows={ 4 }
        variant="standard"
      />
    )
  }
}

NoteField.defaultProps = {
  color: 'primary'
}
export default NoteField
