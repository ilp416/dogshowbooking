import React from "react"
import PropTypes from "prop-types"
import I18n from "i18n-js";
import ReactSafeHtml from "react-safe-html"

import Accordion from '@material-ui/core/Accordion';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionActions from '@material-ui/core/AccordionActions';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Button from '@material-ui/core/Button';
import Divider from '@material-ui/core/Divider';
import TextField from '@material-ui/core/TextField';

import NoteField from './NoteField';
import SelectShowClass from './SelectShowClass';
import PaperWantedField from './PaperWantedField';

const token = document.querySelector('meta[name="csrf-token"]').content;

class RegistrationForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      registration: props.registration,
      errors: {}
    }
  }

  registrationPath() {
    return '/dogs/' + this.props.dog.id + '/registrations'
  }

  checkFieldsFailed() {
    let errFlag = false
    if (!this.state.registration.showClass) {
      this.setState(prevState => ({
        ...prevState,
        errors: {
          ...prevState.errors,
          showClass: ['Нужно выбрать класс']
        }
      }));
      errFlag = true
    }

    if ([null, undefined].includes(this.state.registration.paperWanted)) {
      this.setState(prevState => ({
        ...prevState,
        errors: {
          ...prevState.errors,
          paperWanted: ['Нужно указать нужен каталог или нет']
        }
      }));
      errFlag = true
    }

    return(errFlag);
  }

  handleSubmit(e) {
    e.preventDefault();


    if (this.checkFieldsFailed()) {
      return(false);
    }

    fetch(this.registrationPath(), {
      method: "POST",
      body: JSON.stringify({registration: this.state.registration}),
      headers: {
        'Key-Inflection': 'camel',
        'X-CSRF-Token': token,
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
    })
    .then(response => {
      const ok = response.ok;
      const data = response.json();
      return Promise.all([ok, data]);
    })
    .then(res => {
      return { ok: res[0], data: res[1] }
    })
    .then(result => {
      console.log("reponse :", result);
      if (result.ok) {
        const dog = result.data
        this.props.onChange({});
        // this.setState({ redirect: url });
      }
    })
  }

  attrLabel(attrName) {
    return(I18n.t('activerecord.attributes.registration.' + attrName));
  }

  handleChangeAttr (attr, value) {
    console.log('RegistrationForm attr:' + attr + ', val: ' + value);
    this.setState(prevState => ({
      registration: { ...prevState.registration, [attr]: value },
      errors: { ...prevState.errors, [attr]: undefined }
    }))
  }

  render () {
    console.log('errors');
    console.log(this.state.errors);
    return (
      <Accordion>
        <AccordionSummary
          expandIcon={ <ExpandMoreIcon /> }
          aria-controls="panel1c-content"
          id="panel1c-header"
        >
          <div>
            <h6 className="mb-0 d-flex">
              <i className="bi bi-trophy me-2"></i>
              <ReactSafeHtml html={ this.props.event.name }/>
            </h6>
          </div>
        </AccordionSummary>
        <AccordionDetails>
          <div className="row">
            <div className="col-sm-12 col-md-6">
              <SelectShowClass
                value={ this.state.registration.showClass }
                showClasses= { this.props.event.showClasses }
                disabled={ this.props.disabled }
                onChange={ (e) => this.handleChangeAttr('showClass', e.target.value) }
                error={ this.state.errors.showClass && true }
                errorMsg={
                  this.state.errors.showClass &&
                    this.state.errors.showClass.join(', ')
                }
              />
            </div>
            <div className="col-sm-12 col-md-6">
              <NoteField
                label={ this.attrLabel('note') }
                disabled={ this.props.disabled }
                value={ this.state.registration.note || '' }
                onChange={ (e) => this.handleChangeAttr('note', e.target.value) }
              />
              <Divider variant="middle" className="my-2" />
              <PaperWantedField
                label={ this.attrLabel('paper_wanted') }
                disabled={ this.props.disabled }
                value={ this.state.registration.paperWanted }
                error={ this.state.errors.paperWanted && true }
                errorMsg={
                  this.state.errors.paperWanted &&
                    this.state.errors.paperWanted.join(', ')
                }
                onChange={ (val) => this.handleChangeAttr('paperWanted', val) }
              />
            </div>
          </div>
        </AccordionDetails>
        <AccordionActions>
          <Button
            disabled={ this.props.disabled }
            onClick={ (e) => this.handleSubmit(e) }
            color="primary" variant="contained">
            { I18n.t(this.props.disabled ? 'sent' : 'send') }
          </Button>
        </AccordionActions>
      </Accordion>
    )
  }
}
RegistrationForm.defaultProps = {
  variant: 'standard',
}
export default RegistrationForm
