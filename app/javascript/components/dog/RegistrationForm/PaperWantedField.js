import React from 'react';
import Radio from '@material-ui/core/Radio';
import I18n from "i18n-js";
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import FormHelperText from '@material-ui/core/FormHelperText';

class PaperWantedField extends React.Component {
  humanAttr(attr) {
    return(I18n.t('registration.paper_wanted.' + attr));
  }

  handleChange(event) {
    const result = event.target.value == 'false' ? false : true
    console.log('PaperWantedField:' + result);

    this.props.onChange(result);
  }

  getValue() {
    const val = this.props.value
    if ([undefined, null].includes(val)) {
      return(val)
    }
    return(val.toString())
  }

  render () {
    const {error, errorMsg, ...radioFieldProps} = this.props;
    console.log(radioFieldProps);
    return (
      <FormControl component="fieldset">
        <label className={ this.props.disabled ? 'disabled' : undefined }>
          { this.props.label }:
        </label>
        <RadioGroup row
          { ...radioFieldProps }
          value={ this.getValue() }
          aria-label="gender"
          onChange={ (event) => this.handleChange(event) }
        >
          <FormControlLabel value="true"
            disabled={this.props.disabled}
            control={<Radio color='primary' value="true" />}
            label={this.humanAttr('true')}
          />
          <FormControlLabel value="false"
            disabled={this.props.disabled}
            control={<Radio color='primary' value="false" />}
            label={this.humanAttr('false')}
          />
        </RadioGroup>
        <FormHelperText error>{ errorMsg }</FormHelperText>
        <FormHelperText>{ this.humanAttr('note') }</FormHelperText>
      </FormControl>
    )
  }
}

PaperWantedField.defaultProps = {
  color: 'primary'
}
export default PaperWantedField
