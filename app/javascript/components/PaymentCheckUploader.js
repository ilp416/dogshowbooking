import React from 'react';
import I18n from "i18n-js";

import Button from '@material-ui/core/Button';
import FormHelperText from '@material-ui/core/FormHelperText';

import { DirectUploadProvider } from 'react-activestorage-provider'
import API from 'support/API'

export default function PaymentCheckUploader(props) {
  const [payment, setPayment] = React.useState(props.payment);

  const handleAttachment = (imageBlob) => {
    const newPayment = {...payment, checkImg: imageBlob[0]}
    console.log('payment before send')
    console.log(newPayment)
    setPayment(newPayment)
    API({
      url: '/payments',
      method: 'POST',
      data: { payment: newPayment },
      onSuccess: window.location.reload()
    })
  }

  return (
    <DirectUploadProvider
      onSuccess={handleAttachment}
      render={({ handleUpload, uploads, ready }) => (
        <div>
          <label className="upload_payment_img">
            <input
              hidden
              type="file"
              disabled={!ready}
              onChange={e => handleUpload(e.currentTarget.files)}
            />
            <Button
              component="span"
              variant="contained"
              startIcon={<i className="fas fa-upload" />}
            >
              {props.label}
            </Button>
          </label>

          {uploads.map(upload => {
            switch (upload.state) {
              case 'waiting':
                return <p key={upload.id}>Waiting to upload {upload.file.name}</p>
              case 'uploading':
                return (
                  <p key={upload.id}>
                    Uploading {upload.file.name}: {upload.progress}%
                  </p>
                )
              case 'error':
                return (
                  <p key={upload.id}>
                    Error uploading {upload.file.name}: {upload.error}
                  </p>
                )
              case 'finished':
                return (
                  <p key={upload.id}>Finished uploading {upload.file.name}</p>
                )
            }
          })}
        </div>
      )}
    />
  )
}
