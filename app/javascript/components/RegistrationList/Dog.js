import React from 'react';
import I18n from "i18n-js";

import Link from '@material-ui/core/Link';
import Tooltip from '@material-ui/core/Tooltip';

import Document from './Document'

class Dog extends React.Component {
  clickDogName(e) {
    e.preventDefault();
    this.props.onEditDog(this.props.dog)
  }

  registeredBy() {
    const user = this.props.dog.user;
    if ([{}, undefined].includes(user)) { return null; }
    return(
      <div className="section">
        <i>{I18n.t("activerecord.attributes.dog.registered_by")}: </i>
        <Tooltip title={user.fullname}>
          <span>{user.initials}</span>
        </Tooltip>
        <br />
        <a href={`tel:${user.phone}`}>{user.phone}</a>
        <span> {user.email}</span>
      </div>
    )
  }

  personInfo(key) {
    return (
      <div className="section">
        <i>{I18n.t(`activerecord.attributes.dog.${key}`)}: </i>
        {this.props.dog[key]}
      </div>
    )
  }

  render () {
    const dog = this.props.dog;
    return (
      <div className={"dog-info " + this.props.className}>
        <h5>
          <Link href="#" title={this.props.dog.id} onClick={(e) => this.clickDogName(e)}>
            {this.props.dog.name}
          </Link>
        </h5>
        <div className="dog-infos">
          <span className={ 'sex' + ' ' + dog.sex }>
            {I18n.t(`enumerize.dog.sex.${dog.sex}`)}
          </span>
          <Tooltip title={dog.dateOfBirth}>
            <span className="age">{dog.age}</span>
          </Tooltip>
          <span><Document dog={dog} /></span>
          <span className="color">{I18n.t('dog.color')}: {dog.color}</span>
          { dog.hairType ? (
            <span className="hair-type">{I18n.t('dog.type')}: {dog.hairType}</span>
          ) : null }
          <div>
            { dog.titles }
          </div>
        </div>
        <div className="persons-infos">
          {this.personInfo('breeder')}
          {this.personInfo('owner')}
          {this.registeredBy()}
        </div>
      </div>
    )
  }
}

export default Dog
