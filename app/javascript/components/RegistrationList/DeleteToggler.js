import React from 'react';
import I18n from "i18n-js";

import Icon from '@material-ui/core/Icon';
import IconButton from '@material-ui/core/IconButton';

import API from 'support/API'

export default function DeleteToggler(props) {
  const registration = props.registration

  const registrationUrl = `/admin/registrations/${registration.id}`

  const restoreRegistration = () => {
    API({
      url: registrationUrl,
      method: 'PUT',
      data: { registration: { deletedAt: null } },
      onSuccess: props.onUpdate
    })
  }
  const deleteRegistration = () => {
    API({
      url: registrationUrl,
      method: 'DELETE',
      onSuccess: props.onUpdate
    })
  }

  if (props.disabled) { return(null) }

  return (
    registration.deleted ? (
      <IconButton onClick={restoreRegistration} color="primary">
        <Icon>restore_from_trash</Icon>
      </IconButton>
    ) : (
      <IconButton onClick={deleteRegistration}>
        <Icon>delete</Icon>
      </IconButton>
    )
  )
}
