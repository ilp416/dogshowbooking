import React from 'react';
import I18n from "i18n-js";

import Link from '@material-ui/core/Link';

export default function Document(props) {
  const [documentImage, setDocImage] = React.useState(props.dog.documentImage);
  console.log(props)
  console.log(documentImage)

  return(
    documentImage.path ?
      <Link href={documentImage.path} target="_blank" className="reg-list-dog-doc">
        {props.dog.document}
      </Link>
   :
      <span className="reg-list-dog-doc">
        {props.dog.document}
      </span>
  )
}
