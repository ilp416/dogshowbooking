import React from 'react';
import I18n from "i18n-js";
import Icon from '@material-ui/core/Icon';
import IconButton from '@material-ui/core/IconButton';
import InputLabel from '@material-ui/core/InputLabel';
import InputAdornment from '@material-ui/core/InputAdornment';
import FormControl from '@material-ui/core/FormControl';
import TextField from '@material-ui/core/TextField';
import Input from '@material-ui/core/Input';

import API from 'support/API'

export default function RegistrationItem(props) {
  const [adminNote, setAdminNote] = React.useState(props.registration.adminNote);

  const registrationUrl = `/admin/registrations/${props.registration.id}`

  const isSaved = () => {
    return(props.registration.adminNote === adminNote)
  }

  const saveButton = () => {
    return (
      <InputAdornment position="end" className="align-self-end py-2">
        <IconButton
          aria-label="save note"
          onClick={saveNote}
          edge="end"
          color="primary"
        >
          <Icon>save</Icon>
        </IconButton>
      </InputAdornment>
    )
  }

  const chatIcon = () => {
    return (
      <InputAdornment position="start" className="align-self-start py-2">
        <IconButton edge="start" className="pe-0">
          <Icon fontSize="small">chat_bubble_outline</Icon>
        </IconButton>
      </InputAdornment>
    )
  }

  const saveNote = (e) => {
    API({
      url: registrationUrl,
      method: 'PUT',
      data: { registration: { adminNote: adminNote } },
      onSuccess: props.onUpdate
    })
  }

  return (
    <FormControl fullWidth size="small">
      <Input
        multiline
        label={I18n.t('activerecord.attributes.registration.note')}
        id={`note${props.registration.id}`}
        value={adminNote || ''}
        onChange={(e) => setAdminNote(e.target.value)}
        startAdornment={chatIcon()}
        endAdornment={isSaved() ? undefined : saveButton()}
        fontSize="small"
        className="note-text"
      />
    </FormControl>
  )
}
