import React from 'react';

export default function RegistrationAPI (props) {
  const token = document.querySelector('meta[name="csrf-token"]').content;

  const updateRegShowClass = (value) => {
    fetch(props.url, {
      method: props.method,
      body: JSON.stringify({registration: props.registration}),
      headers: {
        'Key-Inflection': 'camel',
        'X-CSRF-Token': token,
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
    })
    .then(response => {
      const ok = response.ok;
      const data = response.json();
      return Promise.all([ok, data]);
    })
    .then(res => {
      return { ok: res[0], data: res[1] }
    })
    .then(result => {
      console.log("reponse :", result);
      if (result.ok) {
        const data = result.data
        if(typeof props.onSuccess === 'string'){
          window[this.props.onSuccess](data)
        }else if(typeof props.onSuccess === 'function'){
          props.onSuccess(data)
        }else{
          return null;
        }
      } else {
        console.log("reponse ok?:", result.ok);
        console.log("reponse data:", result.data);
      }
    })
  }
}
