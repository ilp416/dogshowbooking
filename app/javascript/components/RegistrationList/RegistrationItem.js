import React from 'react';
import I18n from "i18n-js";

import { ChatBubbleOutlineOutlinedIcon, AnnouncementOutlinedIcon } from '@material-ui/icons';

import SelectShowClass from "./SelectShowClass";
import PriceField from "./PriceField";
import DeleteToggler from "./DeleteToggler";
import PaidToggler from "./PaidToggler";
import PaperGivedOutToggler from "./PaperGivedOutToggler";
import NoteField from "./NoteField";

export default function RegistrationItem(props) {
  const registration = props.registration
  const dog = props.dog
  const deletedClass = registration.deleted ? 'deleted' : null
  const disabled = registration.deleted
  const inactive = !!registration.catalogNumber

  const noteBlock = () => {
    if (!registration.note) { return null }
    return (
      <div className="col comment">
        <i className="fas fa-quote-right text-black-50 pe-2 ps-1" />
        <i> { registration.note }</i>
      </div>
    )
  }

  const linkToCheck = () => {
    if (!registration.payment) { return null }

    return (
      <a className="check-img" href={registration.payment.checkImg.path}>
        {I18n.t("payments.index.check_with_id", {id: registration.payment.id})}
      </a>
    )
  }

  const eventName = () => {
    if (!registration.event) { return null }

    const ev = registration.event

    return(
      <span>
        { ev.club.name + ' [' + ev.prettyDate + '] ' + ev.shortName }
      </span>
    )
  }

  const catalogNumber = () => {
    if (!registration.catalogNumber) { return null }

    return (
      <b> #{registration.catalogNumber}</b>
    )
  }

  return(
    <div className={`registration-item ${deletedClass}`}>
      <div className="title row">
        <div className="col">
          <small>{ registration.event && eventName() }</small>
          { catalogNumber() }
        </div>
      </div>
      <div className="d-flex justify-content-between align-items-center">
        <div className="d-flex actions">
          <SelectShowClass registration={registration} dog={dog}
            disabled={disabled}
            inactive={inactive}
            onSave={props.onUpdate}
            className='pe-1'
          />
          <PriceField registration={registration}
            onUpdate={props.onUpdate}
          />
        </div>

        <PaidToggler
          registration={registration}
          onUpdate={props.onUpdate}
        />

        {linkToCheck()}

        <PaperGivedOutToggler
          registration={registration}
          onUpdate={props.onUpdate}
        />

        <DeleteToggler
          registration={registration}
          onUpdate={props.onUpdate}
          disabled={inactive}
        />
      </div>
      <div className="notes">
        {noteBlock()}
        <NoteField registration={registration} onUpdate={props.onUpdate} />
      </div>
    </div>
  )
}
