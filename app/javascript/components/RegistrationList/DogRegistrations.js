import React from 'react';
import I18n from "i18n-js";

import ListItem from '@material-ui/core/ListItem';

import RegistrationItem from './RegistrationItem';
import Dog from './Dog';

class DogRegistrations extends React.Component {
  render () {
    const dog = this.props.dog;
    return (
      <ListItem key={`dog-${dog.id}`} className="list-group-item p-0">
        <div className="dog-item d-md-flex align-items-start">
          <Dog className="col-md-6 p-3" dog={dog} onEditDog={this.props.onEditDog} />
          <div className="col-md-6 p-3 registrations-info">
            <hr className="d-md-none mb-2" />
            { dog.registrations.map((reg, index) => (
              <div key={`registration-${reg.id}`}>
                <hr className={index < 1 ? "d-none" : "mb-2"} />
                <RegistrationItem key={reg.id} registration={reg} dog={dog}
                  onUpdate={this.props.onUpdate}
                />
              </div>
            ))}
          </div>
        </div>
      </ListItem>
    )
  }
}

export default DogRegistrations
