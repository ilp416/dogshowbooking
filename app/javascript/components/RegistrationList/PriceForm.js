import React, { useRef } from "react";
import I18n from "i18n-js";

import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import Switch from '@material-ui/core/Switch';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import ClickAwayListener from '@material-ui/core/ClickAwayListener';

import { AVAILABLE_CURRENCIES } from 'support/Breeds';
import API from 'support/API'

export default function PriceForm(props) {
  const token = document.querySelector('meta[name="csrf-token"]').content;
  const [value, setValue] = React.useState(props.value);
  const [currency, setCurrency] = React.useState(props.currency);
  const [paid, setPaid] = React.useState(props.paid);

  const paramsForUpdate = () => {
    return({
      registration: {
        price_cents: value * 100,
        price_currency: currency,
        paid: paid
      }
    })
  }

  const updatePrice = () => {
    API({
      url: `/admin/registrations/${props.registration.id}`,
      method: "PUT",
      data: paramsForUpdate(),
      onSuccess: props.onUpdate
    })
  }

  const handleClickAway = (event) => {
    console.log('Click Away');

    props.onClose(event);
  };

  const handleCurrencySelect = (e, value) => {
    e.preventDefault();
    setCurrency(value);
  }

  const handleFocus = (e) => {
    e.target.select();
  };

  return (
    <ClickAwayListener onClickAway={(e) => handleClickAway(e)}>
      <Paper className={'text-center ' + props.className}>
        <div className="price-field text-center">
          <div className="input-group">
            <input type="text"
              className="price form-control form-control-sm border-primary"
              value={value}
              onChange={(e) => setValue(e.target.value)}
              onFocus={handleFocus}
              autoFocus
            />
            <button className="btn btn-outline-primary btn-sm dropdown-toggle"
              data-bs-toggle="dropdown"
              aria-expanded="false"
            >
              {currency}
            </button>
            <ul className="dropdown-menu dropdown-menu-end">
              {AVAILABLE_CURRENCIES.map((curr) => (
                <li key={curr}>
                  <a className="dropdown-item" href="#" onClick={(e) => handleCurrencySelect(e, curr)}>
                    {curr}
                  </a>
                </li>
              ))}
            </ul>
          </div>
        </div>
        <br />
        <small>
          {I18n.t("activerecord.attributes.registration.created_at")}: {props.registration.createdAt}
        </small>
        <br />
        <br />
        <FormControlLabel
          className="mx-0"
          control={
            <Switch
              checked={paid}
              onChange={(e) => setPaid(e.target.checked)}
              name="checkedB"
              color="primary"
              size="small"
            />
          }
          label={I18n.t('registration.paid')}
        />
        <br />
        <Button variant="contained" color="primary" onClick={updatePrice} className="mt-2">
          {I18n.t('save')}
        </Button>
      </Paper>
    </ClickAwayListener>
  )
}
