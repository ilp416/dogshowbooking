import React from 'react';
import I18n from "i18n-js";
import { useEffect } from 'react';

import Button from '@material-ui/core/Button';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import ExpandMoreRoundedIcon from '@material-ui/icons/ExpandMoreRounded';
import Paper from '@material-ui/core/Paper';
import Popover from '@material-ui/core/Popover';
import MenuItem from '@material-ui/core/MenuItem';
import MenuList from '@material-ui/core/MenuList';
import Select from '@material-ui/core/Select';
import Fade from "@material-ui/core/Fade";
import Badge from '@material-ui/core/Badge';

import { withStyles } from '@material-ui/core/styles';

import { AVAILABLE_CURRENCIES } from 'support/Breeds';
import PriceForm from './PriceForm';

const StyledBadge = withStyles((theme) => ({
  badge: {
    zIndex: 4,
    height: '13px',
    fontSize: '0.55rem'
  },
}))(Badge);

export default function PriceField(props) {
  const [open, setOpen] = React.useState(false);
  const registration = props.registration
  const price = registration.price
  const [priceCents, setPriceCents] = React.useState(registration.price.cents)
  const [currencyIso, setCurrencyIso] = React.useState(registration.price.currencyIso)
  const anchorRef = React.useRef(null)
  const isPaid = !!registration.paidAt

  useEffect(
    () => { setOpen(false); },
    [props.registration, props.registration.price]
  );

  const value = () => {
    return(
      priceCents > 0 ? (priceCents / 100) : priceCents
    )
  }

  const handleToggle = () => {
    setOpen((prevOpen) => !prevOpen);
  };

  const handleClose = (event) => {
    if (anchorRef.current && anchorRef.current.contains(event.target)) {
      return;
    }

    setOpen(false);
  };

  const handleUpdate = (reg) => {
    props.onUpdate(reg)
    setOpen(false);
    setPriceCents(reg.price.cents);
    setCurrencyIso(reg.price.currencyIso);
  }

  const badgeColor = () => {
    return (isPaid ? 'primary' : 'error')
  }

  const badgeText = () => {
    const key = isPaid ? 'paid' : 'unpaid'
    return I18n.t(`registration.${key}`)
  }

  const openIfNotDeleted = (e) => {
    if (registration.deleted || props.disabled) { return null }

    setOpen(true)
  }

  return (
    <div className="price-field" ref={anchorRef} >
      <StyledBadge badgeContent={badgeText()} color={badgeColor()} >
        <div className="input-group" onClick={openIfNotDeleted}>
          <button className="btn btn-outline-primary btn-sm text-end price" disabled={props.disabled}>
            {value()}
          </button>
          <button className="btn btn-outline-primary btn-sm dropdown-toggle" disabled={props.disabled}>
            {currencyIso}
          </button>
        </div>
      </StyledBadge>

      <Popover open={open}
        anchorEl={anchorRef.current}
        classes={{root: 'price-form-wrapper'}}
        TransitionComponent={Fade}
      >
        <PriceForm
          currency={currencyIso}
          value={value()}
          registration={registration}
          paid={!!registration.paidAt}
          onUpdate={(reg) => handleUpdate(reg)}
          onClose={(e) => setOpen(false)}
          className="price-form"
        />
      </Popover>
    </div>
  )
}
