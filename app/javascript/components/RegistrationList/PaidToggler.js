import React from 'react';
import IconButton from '@material-ui/core/IconButton';
import API from 'support/API'

export default function PaidToggler(props) {
  const registration = props.registration
  const [paid, setPaid] = React.useState(!!props.registration.paidAt)
  const registrationUrl = `/admin/registrations/${registration.id}`

  const togglePaidRecord = () => {
    API({
      url: registrationUrl,
      method: 'put',
      data: { registration: { paid: !registration.paidAt } },
      onSuccess: props.onUpdate
    })
  }

  if (registration.deleted) { return(null) }

  return (
    <IconButton onClick={togglePaidRecord} className={ !!registration.paidAt ? 'disabled' : 'bg-danger text-white' }>
      <i className="fas fa-hand-holding-usd"></i>
    </IconButton>
  )
}
