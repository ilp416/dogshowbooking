import React from 'react';
import I18n from "i18n-js";
import axios from "axios"

import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListSubheader from '@material-ui/core/ListSubheader';
import TextField from '@material-ui/core/TextField';

import MuiDialogTitle from '@material-ui/core/DialogTitle';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import Typography from '@material-ui/core/Typography';
import Checkbox from '@material-ui/core/Checkbox';
import FormControlLabel from '@material-ui/core/FormControlLabel';

import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';

import { BREEDS } from 'support/Breeds';
import { BREED_LIST } from 'support/Breeds';

import handleViewport from 'react-in-viewport';

import DogRegistrations from './DogRegistrations';
import Form from '../dog/Form';

const Block = (props) => {
  const { forwardedRef } = props;
  return (
    <div className="viewport-block text-center fa-3x" ref={forwardedRef}>
      <i className="fas fa-circle-notch fa-spin d-none"></i>
    </div>
  );
};

const ViewportBlock = handleViewport(Block, /** options: {}, config: {} **/);

class RegistrationList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      dogs: [],
      searchStr: '',
      editDog: {},
      filters: {},
      showedCount: 0
    }
    this.getList = this.getList.bind(this);
  }

  componentDidMount(){
    this.getList();
  }

  getList(reset = true) {
    axios
      .get(
        this.props.source,
        {headers: {'Key-Inflection': 'camel'}}
      )
      .then(response => {
        const dogs = response.data;
        const newShowedCount = reset ? 10 : this.state.showedCount;
        this.setState(prevState => ({
          ...prevState,
          dogs: dogs,
          showedCount: newShowedCount
        }))
      })
      .catch(error => {
        console.log(error);
      })

  }

  humanBreed (breed) {
    const groupStr = I18n.t('groups.' + BREEDS[breed].group);
    return `${I18n.t('breeds.' + breed)} [${groupStr}]`;

  }

  groupedList(count) {
    return this.filtered().slice(0, count).reduce((a, b) => {
      (a[b['breedId']] = a[b['breedId']] || []).push(b);
      return a;
    }, {});
  }

  filtered() {
    return(
      this.filteredIsNotInCatalog(
        this.filteredIsInCatalog(
          this.filteredIsNotPaid(
            this.filteredIsPaid(
              this.filteredisNotChecked(
                this.filteredisChecked(
                  this.filteredDogs(this.state.dogs)
                )
              )
            )
          )
        )
      )
    )
  }

  filteredIsInCatalog(scope) {
    if ([undefined, false].includes(this.state.filters.isInCatalog)) {
      return(scope)
    }

    return(
      scope.filter(dog => {
        let res = dog.registrations && dog.registrations.map((reg) => {
            return(!!reg.catalogNumber == true)
          })
        return(res.includes(true))
      })
    )
  }

  filteredIsNotInCatalog(scope) {
    if ([undefined, false].includes(this.state.filters.isNotInCatalog)) {
      return(scope)
    }

    return(
      scope.filter(dog => {
        let res = dog.registrations && dog.registrations.map((reg) => {
            return(!!reg.catalogNumber == false)
          })
        return(res.includes(true))
      })
    )
  }

  filteredisChecked(scope) {
    if ([undefined, false].includes(this.state.filters.isChecked)) {
      return(scope)
    }

    return(
      scope.filter(dog => {
        let res = dog.registrations && dog.registrations.map((reg) => {
            return(!!reg.payment)
          })
        return(res.includes(true))
      })
    )
  }

  filteredisNotChecked(scope) {
    if ([undefined, false].includes(this.state.filters.isNotChecked)) {
      return(scope)
    }

    return(
      scope.filter(dog => {
        let res = dog.registrations && dog.registrations.map((reg) => {
            return(!reg.payment)
          })
        return(res.includes(true))
      })
    )
  }


  filteredIsPaid(scope) {
    if ([undefined, false].includes(this.state.filters.isPaid)) {
      return(scope)
    }

    return(
      scope.filter(dog => {
        let res = dog.registrations && dog.registrations.map((reg) => {
            return(!!reg.paidAt == true)
          })
        return(res.includes(true))
      })
    )
  }

  filteredIsNotPaid(scope) {
    if ([undefined, false].includes(this.state.filters.isNotPaid)) {
      return(scope)
    }

    return(
      scope.filter(dog => {
        let res = dog.registrations && dog.registrations.map((reg) => {
            return(!!reg.paidAt == false)
          })
        return(res.includes(true))
      })
    )

  }

  filterByNote(dog, searchStr) {
    let res = dog.registrations && dog.registrations.map((reg) => {
        return(
          (reg.adminNote && reg.adminNote.toLowerCase().includes(searchStr)) ||
            ( reg.note && reg.note.toLowerCase().includes(searchStr))
        )
      })
    return(res.includes(true))
  }

  filteredDogs(scope) {
    if ([null, undefined, ''].includes(this.state.searchStr)) {return(scope)}

    const searchStr = this.state.searchStr.toLowerCase();

    return (
      scope.filter(dog => {
        return(
          I18n.t('breeds.' + dog.breedId).toLowerCase().includes(searchStr) ||
            dog.name.toLowerCase().includes(searchStr) ||
              dog.breeder.toLowerCase().includes(searchStr) ||
                dog.owner.toLowerCase().includes(searchStr) ||
                  (dog.user && (
                    (dog.user.initials && dog.user.initials.toLowerCase().includes(searchStr)) ||
                      (dog.user.phone && dog.user.phone.toLowerCase().includes(searchStr)) ||
                        (dog.user.email && dog.user.email.toLowerCase().includes(searchStr))
                  )) || this.filterByNote(dog, searchStr)


        )
      })
    )
  }

  handleChangeSearch(value) {
    this.setState(prevState => ({
      ...prevState,
      searchStr: value,
      showedCount: 10
    }))
  }

  handleEditDogClose() {
    this.setState(prevState => ({
      ...prevState,
      editDog: {}
    }))
  }

  handleEditDog(dog) {
    this.setState(prevState => ({
      ...prevState,
      editDog: dog
    }))
  }

  handleDogUpdated (dog) {
    this.handleEditDogClose()
    this.getList()
  }

  setShowedCount (newCount) {
    this.setState(prevState => ({
      ...prevState,
      showedCount: newCount
    }))
  }

  renderEditDog() {
    if (this.state.editDog.id === undefined) { return null; }
    return (
      <Dialog open onClose={(e) => this.handleEditDogClose()}>
        <MuiDialogTitle>
          <div className="d-flex justify-content-between align-items-center">
            <Typography variant="h6">{I18n.t('dogs.edit.title')}</Typography>
            <IconButton aria-label="close" onClick={(e) => this.handleEditDogClose(e)}>
              <CloseIcon />
            </IconButton>
          </div>
        </MuiDialogTitle>
        <DialogContent>
          <Form
            admin
            dog={this.state.editDog}
            submitButtonTitle={I18n.t('save')}
            onSave={(dog) => this.handleDogUpdated(dog) }
          />
        </DialogContent>
      </Dialog>
    )
  }

  setFilter (filter, value) {
    this.setState(prevState => ({
      ...prevState,
      filters: {
        ...prevState.filters,
        [filter]: value
      }
    }))
  }

  filterItem(filterName) {
    return(
      <FormControlLabel
        control={
          <>
            <Checkbox name={`is-${filterName}`}
              icon={<i className="bi bi-check-square"/>}
              checkedIcon={<i className="bi bi-check-square-fill" />}
              label={`is-${filterName}`}
              onChange={(e) => this.setFilter(`is${filterName}`, e.target.checked)}
            />
            <Checkbox name={`is-not-${filterName}`}
              icon={<i className="bi bi-x-square"/>}
              checkedIcon={<i className="bi bi-x-square-fill" />}
              label={`is-not-${filterName}`}
              onChange={(e) => this.setFilter(`isNot${filterName}`, e.target.checked)}
            />
          </>
        }
        label={I18n.t(`registrations.filters.${filterName}`)}
      />
    )
  }


  render () {
    const list = this.groupedList(this.state.showedCount);

    return (
      <div>
        <div className="filters d-md-flex justify-content-between align-items-end flex-row-reverse">
          <div className="filters d-flex flex-column">
            { this.filterItem('Paid') }
            { this.filterItem('Checked') }
            { this.filterItem('InCatalog') }
          </div>

          <TextField
            label="Поиск"
            onChange={(e) => this.handleChangeSearch(event.target.value)}
            className='mb-2'
          />
        </div>
        <List subheader={<li />} className="registration-list">
          { BREED_LIST.map(breed => {
            if (list[breed.key] === undefined) { return null; }
            return (
              <li key={`section-${breed.key}`}>
                <ul className="list-group">
                  <ListSubheader className="bg-white text-center list-group-item breed-title">
                    <h3>{this.humanBreed(breed.key)}</h3>
                  </ListSubheader>
                  { list[breed.key].map(dog => (
                    <DogRegistrations
                      key={`reg-${dog.id}`}
                      dog={dog}
                      onEditDog={(dog) => this.handleEditDog(dog)}
                      onUpdate={(dog) => this.getList(false)}
                    />
                  ))}
                </ul>
              </li>
            )
          })}
        </List>
        <ViewportBlock
          onEnterViewport={() => this.setShowedCount(this.state.showedCount + 10)}
        />
        { this.renderEditDog() }
      </div>
    );
  }
}

export default RegistrationList
