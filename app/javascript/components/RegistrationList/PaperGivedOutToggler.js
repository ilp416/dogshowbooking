import React from 'react';
import IconButton from '@material-ui/core/IconButton';
import Icon from '@material-ui/core/Icon';
import API from 'support/API'

export default function PaperGivedOutToggler(props) {
  const registration = props.registration
  const [paperGivedOut, setPaperGivedOut] =
    React.useState(!!props.registration.paperGivedOutAt)

  const registrationUrl = `/admin/registrations/${registration.id}`

  const togglePaidRecord = () => {
    API({
      url: registrationUrl,
      data: { registration: { paperGivedOut: !props.registration.paperGivedOutAt }},
      method: 'put',
      onSuccess: props.onUpdate
    })
  }

  const buttonClass = (reg) => {
    if (reg.paperWanted) {
      if (!!reg.paperGivedOutAt) {
        return 'text-success'
      } else {
        return 'bg-warning text-white'
      }
    } else {
      if (!!reg.paperGivedOutAt) {
        return 'bg-info text-white'
      } else {
        return 'disabled'
      }
    }
  }

  if (registration.deleted) { return(null) }

  return (
    <IconButton onClick={togglePaidRecord} className={buttonClass(props.registration)} >
      <Icon className="">menu_book_rounded</Icon>
    </IconButton>
  )
}
