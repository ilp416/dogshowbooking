import React from 'react';
import I18n from "i18n-js";
import { useEffect } from 'react';

import Button from '@material-ui/core/Button';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import ExpandMoreRoundedIcon from '@material-ui/icons/ExpandMoreRounded';
import ClickAwayListener from '@material-ui/core/ClickAwayListener';
import Grow from '@material-ui/core/Grow';
import Paper from '@material-ui/core/Paper';
import Popper from '@material-ui/core/Popper';
import MenuItem from '@material-ui/core/MenuItem';
import MenuList from '@material-ui/core/MenuList';

export default function SelectShowClass(props) {
  const token = document.querySelector('meta[name="csrf-token"]').content;
  const showClasses = props.registration.event.showClasses;

  useEffect(
    () => { setDisabled(props.disabled); },
    [props.registration]
  );

  const handleToggle = () => {
    setOpen((prevOpen) => !prevOpen);
  };

  const handleClose = (event) => {
    if (anchorRef.current && anchorRef.current.contains(event.target)) {
      return;
    }

    setOpen(false);
  };

  const updateRegShowClass = (value) => {
    fetch(`/admin/registrations/${props.registration.id}`, {
      method: "PUT",
      body: JSON.stringify({registration: { showClass: value}}),
      headers: {
        'Key-Inflection': 'camel',
        'X-CSRF-Token': token,
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
    })
    .then(response => {
      const ok = response.ok;
      const data = response.json();
      return Promise.all([ok, data]);
    })
    .then(res => {
      return { ok: res[0], data: res[1] }
    })
    .then(result => {
      console.log("reponse :", result);
      if (result.ok) {
        const dog = result.data
        if(typeof props.onSave === 'string'){
          window[this.props.onSave](dog)
        }else if(typeof props.onSave === 'function'){
          props.onSave(dog)
        }else{
          return null;
        }
      }
    })
  }

  const handleMenuItemClick = (event, value) => {
    setSelectedIndex(value);
    updateRegShowClass(value);
    setOpen(false);
    setDisabled(true);
  };

  const humanValue = (val) => {
    return I18n.t(`enumerize.registration.show_class.${val}`)
  }

  const [open, setOpen] = React.useState(false);
  const [disabled, setDisabled] = React.useState(props.disabled);
  const anchorRef = React.useRef(null);
  const [selectedIndex, setSelectedIndex] = React.useState(1);
  const value = props.registration.showClass;

  return(
    props.inactive ? (
      <div className="btn btn-primary btn-sm show-class" >
        {humanValue(value)}
      </div>
    ) : (
      <div className={props.className}>
        <ButtonGroup
          disabled={disabled}
          variant="contained"
          color="primary"
          ref={anchorRef}
          aria-label="split button"
          size="small"
        >
          <Button>{humanValue(value)}</Button>
          <Button
            color="primary"
            size="small"
            aria-controls={open ? 'split-button-menu' : undefined}
            aria-expanded={open ? 'true' : undefined}
            aria-label="select merge strategy"
            aria-haspopup="menu"
            className="px-0"
            onClick={handleToggle}
          >
            <ExpandMoreRoundedIcon />
          </Button>
        </ButtonGroup>
        <Popper open={open} anchorEl={anchorRef.current} role="button" className="showclass-options" transition>
          {({ TransitionProps, placement }) => (
            <Grow
              {...TransitionProps}
              style={{
                transformOrigin: placement === 'bottom' ? 'center top' : 'center bottom',
              }}
            >
              <Paper>
                <ClickAwayListener onClickAway={handleClose}>
                  <MenuList id="split-button-menu">
                    {showClasses.map((showClass) => (
                      <MenuItem
                        key={showClass}
                        selected={showClass === value}
                        onClick={(event) => handleMenuItemClick(event, showClass)}
                      >
                        { humanValue(showClass) }
                      </MenuItem>
                    ))}
                  </MenuList>
                </ClickAwayListener>
              </Paper>
            </Grow>
          )}
        </Popper>
      </div>
    )
  )
}
