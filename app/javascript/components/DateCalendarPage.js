import React from "react"
import API from 'support/API'
import I18n from "i18n-js";

export default function DateCalendarPage(props) {
  const date = props.date 

  return(
    <div className="date-calendar-page p-3 text-center">
      <div className="day fs-1">
        { I18n.l('date.formats.day', date) }
      </div>
      <div className="month text-uppercase">
        { I18n.l('date.formats.month', date).replace('.', '') }
      </div>
    </div>
  )
}
