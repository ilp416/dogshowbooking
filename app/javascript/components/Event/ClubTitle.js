import React from "react"
import API from 'support/API'
import I18n from "i18n-js";
import ReactSafeHtml from "react-safe-html"

export default function ClubTitle(props) {
  const club = props.club
  const coordinates = props.info.location
  const addrStr = props.info.address

  const address = () => {
    return(
      <ReactSafeHtml html={props.info.address} />
    )
  }
 
  const copyClipboard = (e, str) => {
    e.preventDefault()
    navigator.clipboard.writeText(str)
  }

  return(
    <div className="event-club-title">
      <h5 className="club-name mb-0">
        {club.fullname}
      </h5>
      <div className="phones">
        { club.phones && club.phones.map ((phone, index) => (
          <a key={`club_${club.id}_phone_${index}`}
            className="phone"
            href={`tel: ${phone.replace(/\D/, '')}`}
          >
            {phone}
          </a>
        ))}
      </div>
      { ![null, ''].includes(addrStr) && (
        <div className="address d-flex">
          { address() }
          <a href="#"
             type="button"
             className="clipboard"
             onClick={(e) => copyClipboard(e, addrStr.replaceAll('&nbsp;', ' '))}
          >
            <i className="bi bi-clipboard-plus"/>
          </a>
        </div>
      )}
    </div>
  )
}
