import React from 'react';
import I18n from "i18n-js";

import TextField from '@material-ui/core/TextField';
import PriceField from 'components/Money/PriceField';
import { GROUPED_SHOW_CLASSES } from 'support/Breeds';

import DateField from 'components/Common/DateField';

export default function Prices(props) {
  const jsDefaultDate = new Date(Date.now() + 12096e5).toISOString().split('T')[0];
  const defaultDate = props.defaultDate || jsDefaultDate;
  const [prices, setPrices] = React.useState(props.prices);
  const [dates, setDates] = React.useState(
    Object.keys(prices).length ? Object.keys(prices) : [props.defaultDate]
  )
  const [showClassesLocked, setShowClassLocked] = React.useState(true)
  const [highlighted, setHighlighted] = React.useState(null)

  const changePrices = (oldPrices, date, showClass, country, price) => {
    oldPrices[date] ||= {}
    oldPrices[date][showClass] ||= {}
    oldPrices[date][showClass][country] = price
  }

  const handlePriceChange = (date, showClass, country, price) => {
    let oldPrices = Object.assign({}, prices)
    let classes = showClassesLocked
        ? GROUPED_SHOW_CLASSES[priceGroup(showClass)]
        : [showClass]
    classes.map(sClass => changePrices(oldPrices, date, sClass, country, price))
    setPrices(oldPrices)
  }
  
  const priceFor = (date, showClass, country) => {
    let pricesByDate = prices[date]
    if(pricesByDate == null) { return({}) }
    // while use as react_component it is requred to store data as snake_case
    let camelCaseShowClass = camelize(showClass)
    let pricesByShowClass = pricesByDate[camelCaseShowClass]
    if(pricesByShowClass == null) { return({}) }
    let result = (pricesByShowClass[country] || {})
    return result
  }

  const handleChangeDate = (oldDate, newDate) => {
    const oldPrices = prices
    oldPrices[newDate] ||= oldPrices[oldDate]
    delete oldPrices[oldDate]
    setPrices(oldPrices)
    setDates(Object.keys(prices))
  }

  const addDate = () => {
    setDates(oldDates => [...oldDates, defaultDate])
  }

  const removeDate = (date) => {
    setDates(dates.filter(el => el == date));
  }

  const camelize = (str) => {
    return str.toLowerCase().replace(/[^a-zA-Z0-9]+(.)/g, (m, chr) => chr.toUpperCase())
  }

  const addDateButton = () => {
    return(
      <i onClick={addDate}
         className="bi bi-plus-circle-fill mt-1 ms-2 text-success"
      >
      </i>
    )
  }

  const sortedDates = () => {
    return dates.sort()
  }

  const showClassesLockToggler = () => {
    const icon = showClassesLocked ? 'bi-lock' : 'bi-unlock'
    return(
      <i className={'bi mt-1 ' + icon}
        onClick={() => setShowClassLocked(!showClassesLocked)}
      ></i>
    )
  }

  const lockedIcon = (showClass) => {
    if(!showClassesLocked) { return }

    const icon = GROUPED_SHOW_CLASSES.price1.includes(showClass)
    ? 'bi-file-earmark-lock2'
    : 'bi-file-earmark-lock2-fill'

    return(<i className={'lock-icon bi ' + icon} ></i>)
  }

  const priceGroup = (showClass) => {
    const res = Object.entries(GROUPED_SHOW_CLASSES).find((x) => {
      return x[1].includes(showClass)
    })
    return(!!res && res[0])
  }

  const hlClassName = (showClass) => {
    if(!showClassesLocked) { return 'hl' }
    if(highlighted == null) { return 'hl' }
    return priceGroup(showClass) == highlighted ? 'hl-on' : 'hl-off'
  }

  const handleHl = (showClass) => {
    const val = showClass == null ? null : priceGroup(showClass)
    return setHighlighted(val)
  }

  return(
    <div className="prices-form"> 
      <div className="row g-1 pt-1">
        <div className="col">
          { showClassesLockToggler() }
        </div>
        { sortedDates().map((date, index) => (
          <div className="col d-flex" key={date + '_' + index + '_wrapper'}>
            <span className="p-1">{I18n.t('event.price_until')}</span>
            <DateField
              key={'label_' + date}
              onChange={(newValue) => handleChangeDate(date, newValue)}
              value={date}
              format="dd MMM"
              className="prices-date"
            />
            <i className="bi bi-trash mt-1 ms-2 me-auto text-blank-50"
               onClick={() => removeDate(date)}
            ></i>
            { index == dates.length - 1 && addDateButton() }
          </div>
        ))}
      </div>
      { props.showClasses.map( showClass => (
        <div
          key={'priceRow-' + showClass}
          id={'priceRow-' + showClass}
          className={'row g-1 pt-1 ' + hlClassName(showClass)}
          onMouseEnter={() => handleHl(showClass)}
          onMouseLeave={() => handleHl(null)}
        >
          <div className="col">
            { lockedIcon(showClass) }
            { I18n.t('enumerize.registration.show_class.' + showClass) }
          </div>
          { sortedDates().map((date, index) => (
            <div key={date+'_'+index+'_'+showClass+'_by'} className="col">
              <PriceField 
                fieldName={'event[prices]['+date+']['+showClass+'][by]'}
                price={priceFor(date, showClass, 'by')}
                onChange={(price) => handlePriceChange(date, showClass, 'by', price)}
              />
            </div>
          ))}
        </div>
       ))}
    </div>
  ) 
}
