import React from 'react';
import I18n from "i18n-js";
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';
import FormHelperText from '@material-ui/core/FormHelperText';
import {Link} from 'react-scroll'

export default function CatalogSearch(props) {

  const renderOption = (option) => {
    if (option.type === 'breed') {
    return(
      <Link to={option.key} smooth={true} offset={-300}>
        <h5>{option.object.title}</h5>
      </Link>
    )
    }
    if (option.type === 'reg') {
      return(
        <Link to={option.key} smooth={true} offset={-300}>
          <h5 className="d-block">{option.object.title}</h5>
          <small className="d-block">{option.object.breed}</small>
          <small className="d-block">
            <i>
              <span>Класс: {option.object.showClass}, </span>
              {option.object.gender}
            </i>
          </small>
        </Link>
      )
    }
  }

  return (
    <div>
      <Autocomplete
        options={props.options}
        getOptionLabel={(option) => option.searchStr}
        renderInput={(params) => <TextField {...params} label={props.label}/>}
        renderOption={renderOption}
        blurOnSelect={true}
      />
    </div>
  )
}
