import React from "react"
import PropTypes from "prop-types"
import PhoneInput from 'react-phone-input-2'
import 'react-phone-input-2/lib/material.css'

class PhoneField extends React.Component {
  constructor(props) {
    super(props);
    console.log(props);
    this.state = {}
  }

  render () {
    return (
      <PhoneInput
        value={this.props.value}
        country={'by'}
        preferredCountries={['by', 'ru', 'ua', 'pl', 'lv', 'lt']}
        onChange={phone => this.setState({ phone })}
        specialLabel={this.props.label}
        inputProps={{
          name: this.props.name,
          inputClass: "form-control",
          required: true,
          autoFocus: true
        }}
      />
    );
  }
}

PhoneField.propTypes = {
  name: PropTypes.string,
  value: PropTypes.string
};
export default PhoneField
