import React from 'react';
import I18n from "i18n-js";

import 'date-fns';
import ruLocale from "date-fns/locale/ru";
import DateFnsUtils from '@date-io/date-fns';
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
} from "@material-ui/pickers";

export default function Prices(props) {
  const localeMap = {
    // en: enLocale,
    ru: ruLocale,
  };

  const toSQLDate = (value) => {
    return(value.toJSON().split('T')[0])
  }

  return(
    <MuiPickersUtilsProvider utils={ DateFnsUtils } locale={ localeMap['ru'] }>
      <KeyboardDatePicker
        { ...props }
        cancelLabel={ I18n.t('cancel') }
        okLabel={ I18n.t('ok') }
        autoOk
        onChange={ (value) => props.onChange(toSQLDate(value)) }
      />
    </MuiPickersUtilsProvider>
  )
}
