import React from 'react';

export default function RegistrationAPI (props) {
  const token = document.querySelector('meta[name="csrf-token"]').content;

  fetch(props.url, {
    method: props.method,
    body: JSON.stringify(props.data),
    headers: {
      'Key-Inflection': 'camel',
      'X-CSRF-Token': token,
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
  })
  .then(response => {
    return(
      Promise.all([
        response.ok,
        response.headers.get("Content-Type"),
        response.text()
      ])
    )
  })
  .then(res => {
    console.log("reponse content type:", res[1]);
    console.log("reponse ok?:", res[0]);
    console.log("reponse data:", res[2]);
    const isJson = res[1].includes('json') ? true : false;
    let data = res[2];
    if (isJson && data.length >= 2) { data = JSON.parse(data) }
    return { ok: res[0], data: data }
  })
  .then(result => {
    if (result.ok) {
      const data = result.data
      if(typeof props.onSuccess === 'string'){
        window[props.onSuccess](data)
      }else if(typeof props.onSuccess === 'function'){
        props.onSuccess(data)
      }else{
        return null;
      }
    } else {
      if(typeof props.onFail === 'string'){
        window[props.onFail](data)
      }else if(typeof props.onFail === 'function'){
        props.onFail(data)
      }else{
        return null;
      }
    }
  })
}
