class Payment < ApplicationRecord
  has_one_attached :check_img

  belongs_to :club
  belongs_to :user
  has_many :registrations

  def to_json
    as_json.to_json
  end

  def as_json
    PaymentSerializer.new(self).as_json
  end
end
