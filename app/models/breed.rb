# frozen_string_literal: true

##
# This class represent Breed unit with all properties
class Breed < OpenStruct
  AVAILABLE_PROPS = %i[hair_types colors].freeze

  # Human localized breed name
  def to_s
    I18n.t("breeds.#{key}").mb_chars.titleize.to_s
  rescue
    key
  end

  # Breed name as a symbol
  def to_sym
    key.to_sym
  end

  # keys of available hair types for breed
  def hair_type_keys
    hair_types[:values].map do |value|
      value.is_a?(String) ? value : value.first
    end
  end

  # True if dogs myst be separated by hair type for the event
  def sections_by_hair_type?(event)
    return if hair_types.blank?

    hair_types[:all_show_specs] || hair_types[:mono_show_specs] && event.mono?
  end

  # True if dogs myst be separated by color for the event
  def sections_by_color?(event)
    return if colors.blank?

    colors[:all_show_specs] || colors[:mono_show_specs] && event.mono?
  end

  # Hair types structure for catalog
  # {
  #   "hairless" => "hairless",
  #   "powder_puff" => "powder_puff"
  # }
  def catalog_hair_types_hash
    catalog_breed_prop_hash :hair_types
  end

  # Colors structure for catalog
  # for simple colors:
  # {
  #   "pepper_and_salt" => "pepper_and_salt",
  #   "pure_black" => "pure_black"
  # }
  # for grouped colors (poodle_standard):
  # {
  #   "classic" => [:brown, :black, :white],
  #   "modern" => [:silver, :apricot]
  # }
  def catalog_colors_hash
    catalog_breed_prop_hash :colors
  end

  def catalog_title
    if id
      I18n.t 'catalogs.show.fci_breed_title', id: id, breed: to_s
    else
      to_s
    end
  end

  # Hash of possible values for a property of a Breed
  # Used for selectboxes. For poodle_standard:
  # [
  #   [:brown, "brown"],
  #   [:black, "black"],
  #   [:white, "white"],
  #   [:silver, "silver"],
  #   [:apricot, "apricot"]
  # ]
  def human_prop_and_ids(prop, id_first: true)
    prop_values(prop).map do |value|
      key = value.is_a?(Array) ? value.first : value
      t_key = "#{prop}.#{key}"
      next unless I18n.exists? t_key

      item = [key, I18n.t(t_key)]
      id_first ? item : item.reverse
    end.compact
  end

  def available_color_ids
    prop_values(:colors).map do |value|
      value.is_a?(Array) ? value.first : value.to_sym
    end
  end

  def available_hair_type_ids
    prop_values(:hair_types).map do |value|
      value.is_a?(Array) ? value.first : value.to_sym
    end
  end

  def self.all
    BREED_LIST.keys.map { |key| find key }
  end

  def self.keys
    BREED_LIST.keys
  end

  def self.find(key)
    new BREED_LIST[key.to_sym]
  end

  def self.sorted_list(forced: false)
    key = "breeds_sorted_list_#{I18n.locale}"
    Rails.cache.delete key if forced
    Rails.cache.fetch(key) do
      res = BREED_LIST.values.sort_by do |i|
        [
          I18n.t("group_ids.#{i[:group]}"),
          I18n.t("breeds.#{i[:key]}")
        ]
      end
      puts res if forced
      res
    end
  end

  def self.colors_hash
    all.map { |breed| [breed.key.to_sym, breed.available_color_ids] }.to_h
  end

  def color_ids
    return [] if colors.blank?

    values = colors[:values]
    res = values.is_a?(Hash) ? values.keys : values
    res.map(&:to_s)
  end

  def hair_type_ids
    return [] if hair_types.blank?

    values = hair_types[:values]
    res = values.is_a?(Hash) ? values.keys : values
    res.map(&:to_s)
  end

  def self.hair_types_hash
    all.map { |breed| [breed.key.to_sym, breed.available_hair_type_ids] }.to_h
  end

  def color_group(value)
    return unless value

    props_and_groups(:colors)[value.to_sym]
  end

  private

  def props_and_groups(prop)
    data = prop_values(prop)
    return Hash[data.map { |i| [i.to_sym, i] }] if data.is_a? Array
    data
  end


  def catalog_breed_prop_hash(prop)
    res = breed_prop_map_values(prop_values(prop))
          .sort_by { |key, _val| I18n.t "#{prop}.#{key}" }

    res.to_h
  end

  def breed_prop_map_values(values)
    case values
    when Hash
      values.group_by { |_key, val| val }
            .map { |key, val| [key, val.map(&:first)] }
    when Array
      values.map { |val| [val, val] }
    end
  end

  def prop_values(prop)
    raise "Unknown property: #{prop}" unless prop.to_sym.in? AVAILABLE_PROPS

    prop_data = send(prop)
    return [] if prop_data.blank?

    prop_data[:values]
  end
end
