# frozen_string_literal: true

class User < ApplicationRecord
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable,
         # :validatable,
         # :confirmable,
         :lockable, :trackable, :masqueradable,
         :omniauthable, omniauth_providers: %i[google_oauth2 facebook vkontakte]
  has_many :dogs
  has_many :registrations

  has_many :roles
  has_many :roleable_clubs, class_name: 'Club',
                            through: :roles, source: :roleable, source_type: 'Club'
  has_many :roleable_events, class_name: 'Event',
                             through: :roles, source: :roleable, source_type: 'Event'
  has_many :payments

  # has_many :adminable_clubs,

  validates :email, presence: true, uniqueness: { case_sensitive: false }
  validates :phone, phone_number: true, allow_blank: false
  validates :first_name, presence: true
  validates :last_name, presence: true
  validates :password, presence: true, on: :create
  validates :password, confirmation: true,
                       unless: -> { password.blank? }, on: :create

  def self.from_omniauth(access_token)
    data = access_token.info
    user = User.find_by email: data['email']
    user || create_from_omniauth(data)
  end

  def adminable_clubs
    return Club.all if superadmin?

    roleable_clubs
  end

  def adminable_event_ids
    return Event.all.pluck(:id) if superadmin?
    [adminable_clubs_event_ids + roleable_event_ids].flatten.uniq
  end

  def adminable_events
    Event.where id: adminable_event_ids
  end

  def adminable_clubs_event_ids
    Event.where(club_id: roleable_club_ids).pluck(:id)
  end

  def phone=(value)
    super Phoner::Phone.parse(value).to_s
  rescue StandardError
    super value
  end

  def fullname
    [last_name, first_name, second_name].compact * ' '
  end

  def initials
    return if last_name.blank?

    name_part = "#{first_name[0]&.capitalize}."
    second_name_part = second_name.present? ? "#{second_name[0]&.capitalize}." : nil
    ([last_name, name_part, second_name_part].compact * ' ')
      .gsub(/\b('?\W)/) { Regexp.last_match(1).capitalize }
  end

  def superadmin?
    # email == 'ilp416@gmail.com'
    name == 'superadmin'
  end

  def privileged?
    superadmin? || roles.any?
  end

  def self.create_from_omniauth(data)
    my_logger = Logger.new("#{Rails.root}/log/oauth.log")
    my_logger.info("Creating user with data: #{data}")
    user = User.new(
      name: data[:name],
      first_name: data[:first_name],
      last_name: data[:last_name],
      email: data[:email],
      password: Devise.friendly_token[0, 20],
      avatar: data[:image],
      confirmed_at: data[:email_verified] == false ? nil : Time.zone.now
    )
    user.save validate: false
    user
  end
end
