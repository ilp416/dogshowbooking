# frozen_string_literal: true

class Club < ApplicationRecord
  has_many :roles, as: :roleable

  serialize :phones
end
