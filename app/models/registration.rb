# frozen_string_literal: true

class Registration < ApplicationRecord
  extend Enumerize

  belongs_to :dog
  belongs_to :event
  belongs_to :user, optional: true
  belongs_to :payment, optional: true

  enumerize :show_class,
            in: %i[
              baby puppy
              junior intermediate open winner work champ veteran
              detection
              teen young middle working
            ], scope: true

  AVAILABLE_CURRENCIES = %i[BYN RUB EUR USD].freeze
  monetize :price_cents, with_model_currency: :price_currency

  before_create :set_catalog_number
  before_create :set_price

  scope :deleted, -> { where.not deleted_at: nil }
  scope :exists, -> { where deleted_at: nil }
  scope :paid, -> { where.not paid_at: nil }
  scope :unpaid, -> { where paid_at: nil }
  scope :additional_list, -> { where additional_list: true }
  scope :main_list, -> { where additional_list: [false, nil] }

  def paid=(value)
    self.paid_at = value == true ? Time.zone.now : nil
  end

  def paid?
    !!paid_at
  end

  def paper_gived_out=(value)
    self.paper_gived_out_at = value == true ? Time.zone.now : nil
  end

  def destroy
    update deleted_at: Time.zone.now
  end

  def deleted
    deleted_at.present?
  end

  def set_catalog_number
    max_num = Registration.where(event_id: event_id).maximum(:catalog_number)
    return unless max_num

    self.catalog_number = max_num + 1
    self.additional_list = true
  end

  def set_price
    self.price = Registrations::DetectPrice.call self
  end

  def human_price
    "#{price.to_i} #{price.currency.id.upcase}"
  end

  def show_class_short
    I18n.t "show_classes_short.#{show_class}"
  end

  def self.grouped_show_classes
    {
      price1: %i[baby puppy veteran teen],
      price2: %i[junior intermediate open winner work champ detection],
      price3: %i[young middle],
      price4: %i[working]
    }
  end
end
