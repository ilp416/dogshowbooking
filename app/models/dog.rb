# frozen_string_literal: true

class Dog < ApplicationRecord
  include ActiveModel::Serializers::JSON
  extend Enumerize

  DOCUMENT_ALTERNATIVES = %i[puppy_card exchange].freeze

  has_one_attached :doc_img

  has_many :registrations
  has_many :events, through: :registrations
  belongs_to :user, optional: true

  # validates :doc_img, attached: true, if: :document_is_pedigree?
  validates :name, uniqueness:
    { scope: %i[breed_id date_of_birth user_id], message: :not_uniq }

  validates :breed_id, inclusion: { in: Breed.keys.map(&:to_s) }

  validates :name, :document, :owner, :sex, :breeder,
            :mother_name, :father_name, presence: true

  validates :color_str, presence: true, unless: :color_id
  validates :color_id, presence: true, unless: :color_str

  before_validation :normalize_color

  validate :date_of_birth_in_the_past
  validate :hair_type_right_value
  validate :color_id_right_value

  scope :available_sexes, -> { sex.values | group(:sex).pluck(:sex) }

  enumerize :sex, in: %i[male female], scope: true

  strip_attributes collapse_spaces: true, replace_newlines: true

  def url
    "/dogs/#{id}"
  end

  def breed
    return if breed_id.blank?

    I18n.t("breeds.#{breed_id}")
  end

  def date_of_birth_in_the_past
    return if date_of_birth.present? && date_of_birth.past?

    errors.add :date_of_birth, :wrong
  end

  def hair_type_right_value
    return if breed.blank?

    if breed_obj.hair_types.blank?
      self.hair_type_id = nil
      return
    end
    return if hair_type_id.to_s.in? breed_obj.hair_type_ids

    errors.add :hair_type_id, :wrong
  end

  def color_id_right_value
    return if breed.blank?
    return if breed_obj.colors.blank?
    return if color_id.to_s.in? breed_obj.color_ids

    errors.add :color_id, :wrong
  end

  def color
    if color_id.blank?
      return unless color_str

      if I18n.exists? "colors.#{color_str}"
        I18n.t "colors.#{color_str}"
      else
        color_str
      end
    else
      I18n.t("colors.#{color_id}")
    end
  end

  def hair_type
    return if hair_type_id.blank?

    I18n.t("hair_types.#{hair_type_id}")
  end

  def breed_obj
    return if breed_id.blank?

    Breed.find breed_id
  end

  def opened_registrations
    registrations.joins(:event).merge(Event.not_past)
  end

  def color_str=(value)
    super value&.mb_chars&.downcase
  end

  def name=(value)
    super value&.mb_chars&.titleize
  end

  def normalize_color
    return self.color_str = nil if color_id.present?
    return if color_str.blank?

    formatted_val = color_str&.mb_chars&.downcase.to_s.strip
    color_id = I18n.t('colors').key(formatted_val)
    if color_id.present?
      self.color_id = color_id
      self.color_str = nil
    else
      self.color_str = formatted_val
    end
  end

  def mother_name=(value)
    super value&.mb_chars&.titleize
  end

  def father_name=(value)
    super value&.mb_chars&.titleize
  end

  def breeder=(value)
    super value&.mb_chars&.titleize
  end

  def owner=(value)
    super value&.mb_chars&.titleize
  end

  def color_group_id
    breed_obj.color_group(color_id)
  end

  def human_color_group
    return '' unless color_id

    group_key = "color_groups.#{breed_obj.color_group(color_id)}"
    return I18n.t group_key if I18n.exists? group_key

    I18n.t "colors.#{color_id}"
  end

  def human_hair_type
    return '' unless hair_type_id

    key = "hair_types.#{hair_type_id}"
    I18n.exists?(key) ? I18n.t(key) : ''
  end

  def male?
    sex.to_sym == :male
  end

  def female?
    !male?
  end

  def gender
    sex
  end

  def age
    return unless date_of_birth

    count = ((Date.today - date_of_birth) / 365 * 12).round
    if count < 24
      key = 'x_months'
    else
      count /= 12
      key = 'x_years'
    end

    I18n.t("datetime.distance_in_words.#{key}", count: count)
  end

  def document_is_pedigree?
    return false if document.blank?

    !document.to_sym.in? DOCUMENT_ALTERNATIVES
  end

  def human_document
    return document if document_is_pedigree?

    I18n.t("dogs.document_alts.#{document}")
  end

  def to_json
    DogSerializer.new(self).to_json
  end
end
