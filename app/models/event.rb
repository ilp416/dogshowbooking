# frozen_string_literal: true

class Event < ApplicationRecord
  extend Enumerize

  serialize :prices

  has_many :registrations
  has_many :dogs, through: :registrations
  has_many :users, through: :registrations
  has_many :roles, as: :roleable
  belongs_to :club

  has_one_attached :location_map
  has_one_attached :printable_catalog
  has_one_attached :printable_catalog_with_prices
  enumerize :rank, in: %i[universal mono champ], scope: true
  enumerize :catalog_status, in: { missing: 0, processing: 1, done: 5 },
    default: :missing

  scope :future, -> { where 'starts_on > CURRENT_DATE' }
  scope :past, -> { where 'starts_on < CURRENT_DATE' }
  scope :not_past, -> { where 'starts_on >= CURRENT_DATE' }
  scope :opened, -> { where 'registration_closed_at > CURRENT_TIMESTAMP' }

  def universal?
    rank == 'universal'
  end

  def mono?
    !universal?
  end

  def url
    "/events/#{id}"
  end

  def root_event
    Event.where(club_id: club_id, starts_on: starts_on)
         .order(:id)
         .first
  end

  def catalog_published?
    catalog_published_at&.past? && !catalog_hidden_for_all
  end

  def show_full_dogs_data?
    catalog_published?
  end

  def show_classes
    ShowClassesList.call self
  end

  def self.available_for_dog(dog)
    breed_obj = dog.breed_obj
    where(
      'rank = ? OR subject IN(?) OR subject ILIKE(?)',
      :universal,
      [breed_obj.group, breed_obj.alt_group, breed_obj.alt_group_2].flatten,
      "%:#{dog.breed_id},%"
    )
  end
end
