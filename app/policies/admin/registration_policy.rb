# frozen_string_literal: true

module Admin
  class RegistrationPolicy < Admin::ApplicationPolicy
    class Scope
      def resolve
        return scope if user.superadmin?

        scope.where(event_id: adminable_event_ids)
      end
    end

    attr_reader :user, :registration

    def update?
      adminable?
    end

    def destroy?
      adminable?
    end

    def club_admin?
      @record.event.club_id == @user.adminable_clubs
    end

    def event_admin?
      @record.event_id == @user.adminable_events
    end

    private

    def adminable?
      record.event_id.in? adminable_event_ids
    end

    def adminable_event_ids
      @user.adminable_event_ids
    end
  end
end
