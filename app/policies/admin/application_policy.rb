# frozen_string_literal: true

module Admin
  class ApplicationPolicy < ApplicationPolicy
    private

    def superadmin?
      @user.superadmin?
    end
  end
end
