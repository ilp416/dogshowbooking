# frozen_string_literal: true

module Admin
  class EventPolicy < Admin::ApplicationPolicy
    class Scope
      def resolve
        return scope if user.superadmin?

        scope.where(id: user.adminable_event_ids)
      end
    end
  end
end
