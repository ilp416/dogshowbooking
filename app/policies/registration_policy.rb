# frozen_string_literal: true

class RegistrationPolicy < ApplicationPolicy
  attr_reader :user, :registration

  def create?
    true
  end
end
