# frozen_string_literal: true

json.date_registrations @registrations.group_by { |r| r.event.starts_on } do |date, date_regs|
  json.date date
  json.clubs_registrations date_regs.group_by { |r| r.event.club_id } do |club_id, club_regs|
    json.club_id club_id
    json.info club_regs.first.event.root_event, :address, :location
    json.registrations club_regs, :id, :dog_id, :show_class, :event_id,
                                  :paid_at, :note, :paper_wanted, :admin_note
  end
end

json.clubs do
  @clubs.each do |club|
    json.set! club.id do
      json.(club, :id, :name, :fullname, :slug, :phones)
    end
  end
end

json.events do
  @events.each do |event|
    json.set! event.id do
      json.(event, :id, :name, :short_name, :starts_on, :show_classes)
    end
  end
end
