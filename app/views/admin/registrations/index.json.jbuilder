# frozen_string_literal: true

json.array! @dog_registrations do |dog|
  json.extract! dog, :id, :name, :breed_id, :document,
    :date_of_birth, :age, :sex,
    :color_id, :color_str, :color, :hair_type_id, :hair_type,
    :mother_name, :father_name, :breeder, :owner, :titles, :chip, :stigma

  json.document_image do
    doc_img = dog.doc_img
    if doc_img.attached?
      json.name doc_img.filename
      json.path Rails.application.routes.url_helpers.rails_blob_path(
          doc_img, only_path: true
        )
    else
      json.name nil
      json.path nil
    end
  end

  json.registrations do
    json.array! dog.registrations do |reg|
      json.extract! reg, :id, :dog_id, :show_class, :event_id,
        :paid_at, :price, :admin_note, :note, :paper_wanted,
        :admin_note, :deleted, :catalog_number,
        :paper_gived_out_at

      json.created_at reg.created_at.to_date

      json.event do
        json.extract! reg.event, :id, :name, :short_name, :starts_on, :show_classes
        json.club do
          json.extract! reg.event.club, :id, :name
        end

        json.pretty_date l(reg.event.starts_on, format: :pretty)
      end


      if reg.payment && reg.payment.check_img.attached?
        check_img = reg.payment.check_img
        json.payment do
          json.id reg.payment.id
          json.check_img do
            json.filename check_img.filename
            json.path rails_blob_path(check_img)
          end
        end
      end
    end
  end

  json.user_id dog.registrations.last.user_id

  json.user do
    next unless dog.user
    json.extract! dog.user, :email, :initials, :phone, :fullname
  end
end
