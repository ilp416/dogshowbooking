# frozen_string_literal: true
json.extract! @registration, :id, :dog_id, :show_class, :event_id,
  :paid_at, :price, :admin_note, :note, :paper_wanted, :admin_note,
  :deleted, :created_at, :paper_gived_out_at

  json.created_at @registration.created_at.to_date

json.event do
  json.extract! @registration.event, :id, :name, :short_name
end
