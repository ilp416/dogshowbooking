# frozen_string_literal: true
#
json.extract! @event, :id, :name, :rank
json.registrations do
  json.array! @registrations do |reg|
    dog = reg.dog
    breed = dog.breed_obj

    json.extract! reg, :id, :dog_id, :show_class, :event_id,
      :paid_at, :price, :admin_note, :note, :paper_wanted,
      :admin_note, :deleted, :catalog_number
    json.group_id breed.group

    json.event do
      json.extract! reg.event, :id, :name, :short_name, :starts_on, :show_classes
      json.club do
        json.extract! reg.event.club, :id, :name
      end
      json.pretty_date l(reg.event.starts_on, format: :pretty)
    end

    json.dog do
      json.extract! dog, :id, :name, :breed_id, :document,
        :date_of_birth, :age, :sex, :gender,
        :color_id, :color_str, :color, :hair_type_id, :hair_type,
        :mother_name, :father_name, :breeder, :owner
      json.document_image do
        doc_img = dog.doc_img
        if doc_img.attached?
          json.name doc_img.filename
          json.path Rails.application.routes.url_helpers.rails_blob_path(
              doc_img, only_path: true
            )
        else
          json.name nil
          json.path nil
        end
      end
      json.human_color_group(
        breed.sections_by_color?(@event) ? dog.human_color_group : ''
      )
      json.human_hair_type_group(
        breed.sections_by_hair_type?(@event) ? dog.human_hair_type : ''
      )

      json.user do
        next unless dog.user
        json.extract! dog.user, :email, :initials, :phone, :fullname
      end
    end
  end
end
