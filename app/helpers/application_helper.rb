# frozen_string_literal: true

module ApplicationHelper
  def form_error(model, field)
    field_errors = model.errors.messages[field.to_sym]
    return if field_errors.blank?

    content_tag(:div, class: 'form-text') do
      field_errors.each do |err|
        concat content_tag :div, err, class: 'text-danger'
      end
    end
  end

  def today_events
    Event.where(starts_on: Date.today, catalog_hidden_for_all: false)
         .group_by(&:club_id)
  end

  def adminable_events
    events = if current_user.superadmin?
        Event.all
      else
        Event.where id: current_user.adminable_event_ids
      end
    events.includes(:club).order(starts_on: :DESC, created_at: :ASC)
  end

  def payments_count
    current_user.registrations
      .unpaid
      .where('price_cents > 0')
      .joins(:event)
      .merge(Event.future)
      .group('club_id', 'events.starts_on')
      .pluck('club_id', 'events.starts_on')
      .count
  end

  def grouped_club_events
    Event.joins(:club)
         .group(:starts_on, :club_id, 'club_name')
         .order(starts_on: :DESC, club_id: :ASC)
         .pluck(:starts_on, 'clubs.name as club_name', 'array_agg(events.id)')
  end

  def phone_for_url(str)
    Phoner::Phone.parse(str).to_s
  end
end
