# frozen_string_literal: true

class ApplicationMailer < ActionMailer::Base
  default from: 'MyDog.Show <no-reply@mydog.show>'
  layout 'mailer'
end
